package com.sollasi.IPG;

import com.sollasi.IPG.provider.payir.PayIrRestTemplate;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.ws.client.core.WebServiceTemplate;

import javax.net.ssl.SSLContext;
import java.security.NoSuchAlgorithmException;

@Configuration
public class IPGConfiguration {

    @Value("${IPG.url}")
    private String IPGUrl;


    @Value("${IPG.provider}")
    private String provider;

    /*@Bean
    public Jaxb2Marshaller marshaller() {
        final Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("com.sollasi.IPG.provider." + provider);
        return marshaller;
    }
*/
    @Bean
    public WebServiceTemplate getWebServiceTemplate() {
        //WebServiceTemplate webServiceTemplate = new WebServiceTemplate();
        //webServiceTemplate.setDefaultUri(IPGUrl);
        //webServiceTemplate.setMarshaller(marshaller());
        //webServiceTemplate.setUnmarshaller(marshaller());
        //return webServiceTemplate;
        return null;
    }

    @Bean
    @Primary
    public IPGClient getIPGClient() {
        if (provider.equals("arianpal")) {
            return new ArianpalClient();
        } else if (provider.equals("payIr")) {
            return new PayIrClient();
        }
        return null;
    }


    @Bean
    public IPGErrorHandler IPGRestTemplateErrorHandler() {
        return new IPGErrorHandler();
    }

    @Bean
    public RestTemplate IPGRestTemplate() throws NoSuchAlgorithmException {
        RestTemplate restTemplate = null;
        if (provider.equals("payIr")) {
            restTemplate = new PayIrRestTemplate();
            restTemplate.setRequestFactory(this.getClientHttpRequestFactory());
            restTemplate.setErrorHandler(this.IPGRestTemplateErrorHandler());
        }

        return restTemplate;
    }

    private ClientHttpRequestFactory getClientHttpRequestFactory() throws NoSuchAlgorithmException {
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();

        clientHttpRequestFactory.setConnectTimeout(10000);
        clientHttpRequestFactory.setReadTimeout(10000);

        clientHttpRequestFactory.setHttpClient(this.getHttpClient());

        return clientHttpRequestFactory;
    }

    private CloseableHttpClient getHttpClient() throws NoSuchAlgorithmException {

        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
                SSLContext.getDefault(),
                new String[]{"TLSv1", "TLSv1.1", "TLSv1.2"},
                null,
                (hostname, session) -> true
        );

        final Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", new PlainConnectionSocketFactory())
                .register("https", sslsf)
                .build();

        PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager(registry);

        connManager.setMaxTotal(250);
        connManager.setDefaultMaxPerRoute(225);
        connManager.setValidateAfterInactivity(1000);

        return HttpClients.custom()
                .setConnectionManager(connManager)
                .build();
    }


}