package com.sollasi.IPG;

import com.sollasi.IPG.exception.IPGException;
import com.sollasi.IPG.provider.payir.response.InitiateResponse;
import com.sollasi.IPG.request.IPGStepOneRequest;
import com.sollasi.IPG.request.IPGStepTwoRequest;
import com.sollasi.IPG.response.IPGStepOneResponse;
import com.sollasi.IPG.response.IPGStepTwoResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.conn.ConnectTimeoutException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
class PayIrClient implements IPGClient {

    @Value("${IPG.key}")
    private String password;

    @Value("${IPG.url}")
    private String baseUrl = "https://pay.ir/payment";

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public IPGStepOneResponse initiate(IPGStepOneRequest request) throws IPGException {
        try {
            Map<String, String> params = new HashMap<String, String>() {{
                put("api", password);
                put("amount", String.valueOf(request.getAmount()));
                put("redirect", request.getReturnPath());
                put("mobile", request.getPhoneNumber());
                put("factorNumber", String.valueOf(request.getPaymentId()));
            }};
            InitiateResponse result = restTemplate.postForObject(baseUrl + "/send?" +
                            "api={api}&" +
                            "amount={amount}&" +
                            "redirect={redirect}&" +
                            "mobile={mobile}&" +
                            "factorNumber={factorNumber}",
                    null,
                    InitiateResponse.class, params);

            IPGStepOneResponse response = new IPGStepOneResponse();
            response.setPaymentPath(baseUrl + "/gateway/" + result.getTransId());
            return response;
        } catch (RestClientException e) {
            if (e.getRootCause() instanceof SocketTimeoutException) {

            } else if (e.getRootCause() instanceof ConnectTimeoutException) {


            } else {

            }
        }
        return null;

    }

    @Override
    public IPGStepTwoResponse verify(IPGStepTwoRequest request) throws IPGException {
/*
        IPGStepTwoResponse response = new IPGStepTwoResponse();
        response.setAmount(result.getVerifyPaymentResult().getPayementedPrice().longValue());
        return response;*/
        return null;
    }
}