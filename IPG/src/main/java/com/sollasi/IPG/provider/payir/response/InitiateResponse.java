package com.sollasi.IPG.provider.payir.response;

import lombok.Data;

@Data
public class InitiateResponse {
    private int status;
    private String transId;
}
