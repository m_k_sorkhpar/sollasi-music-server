package com.sollasi.IPG.exception;

public class IPGException extends Exception {

    public IPGException(String message) {
        super(message);
    }

}
