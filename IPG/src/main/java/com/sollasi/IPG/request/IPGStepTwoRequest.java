package com.sollasi.IPG.request;

import lombok.Data;

@Data
public class IPGStepTwoRequest {

    private Long price;
    private String IPGReference;

}
