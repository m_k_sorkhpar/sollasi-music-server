package com.sollasi.IPG.request;

import lombok.Data;

@Data
public class IPGStepOneRequest {

    private String phoneNumber;

    private Long amount;

    private Long paymentId;

    private String returnPath;

}
