package com.sollasi.IPG.response;

import lombok.Data;

@Data
public class IPGStepOneResponse {

    private String paymentPath;

}
