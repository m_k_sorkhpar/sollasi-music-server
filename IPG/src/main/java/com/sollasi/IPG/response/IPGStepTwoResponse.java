package com.sollasi.IPG.response;

import lombok.Data;

@Data
public class IPGStepTwoResponse {

    private long amount;

}
