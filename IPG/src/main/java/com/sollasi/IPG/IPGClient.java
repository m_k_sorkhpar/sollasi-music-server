package com.sollasi.IPG;

import com.sollasi.IPG.exception.IPGException;
import com.sollasi.IPG.request.IPGStepOneRequest;
import com.sollasi.IPG.request.IPGStepTwoRequest;
import com.sollasi.IPG.response.IPGStepOneResponse;
import com.sollasi.IPG.response.IPGStepTwoResponse;

public interface IPGClient {

    IPGStepOneResponse initiate(IPGStepOneRequest request) throws IPGException;

    IPGStepTwoResponse verify(IPGStepTwoRequest request) throws IPGException;
}
