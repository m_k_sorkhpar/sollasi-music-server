package com.sollasi.IPG;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sollasi.IPG.provider.payir.error.PayIrError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;

import java.io.IOException;

@Slf4j
public class IPGErrorHandler extends DefaultResponseErrorHandler {

    private ObjectMapper mapper;


    @Value("${IPG.provider}")
    private String provider;

    @Autowired
    public void setMapper(ObjectMapper mapper) {
        this.mapper = mapper;
    }


    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        if (provider.equals("payIr")) {
            PayIrError exceptionDto = mapper.readValue(response.getBody(), PayIrError.class);
            throw new IOException("IPG_ERROR");
        }
    }

    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        return (response.getStatusCode() != HttpStatus.OK);
    }

}