package com.sollasi.IPG;

import com.sollasi.IPG.exception.IPGException;
import com.sollasi.IPG.exception.IPGExceptionCodes;
import com.sollasi.IPG.provider.arianpal.*;
import com.sollasi.IPG.request.IPGStepOneRequest;
import com.sollasi.IPG.request.IPGStepTwoRequest;
import com.sollasi.IPG.response.IPGStepOneResponse;
import com.sollasi.IPG.response.IPGStepTwoResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.SoapMessage;

import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.math.BigDecimal;

@Service
@Slf4j
class ArianpalClient implements IPGClient {

    @Value("${IPG.username}")
    private String username;

    @Value("${IPG.key}")
    private String password;

    @Autowired
    private WebServiceTemplate webServiceTemplate;

    @Override
    public IPGStepOneResponse initiate(IPGStepOneRequest request) throws IPGException {

        RequestPayment requestPayment = new ObjectFactory().createRequestPayment();
        requestPayment.setMerchantID(username);
        requestPayment.setPassword(password);
        requestPayment.setMobile(request.getPhoneNumber());
        requestPayment.setPrice(new BigDecimal(request.getAmount()));
        requestPayment.setResNumber(String.valueOf(request.getPaymentId()));
        requestPayment.setReturnPath(request.getReturnPath());


        RequestPaymentResponse result = (RequestPaymentResponse)
                webServiceTemplate.marshalSendAndReceive(requestPayment,
                        new WebServiceMessageCallback() {
                            @Override
                            public void doWithMessage(WebServiceMessage webServiceMessage) throws IOException, TransformerException {
                                ((SoapMessage) webServiceMessage).setSoapAction("http://tempuri.org/RequestPayment");
                            }
                        });


        switch (result.getRequestPaymentResult().getResultStatus()) {
            case SUCCEED:
                IPGStepOneResponse response = new IPGStepOneResponse();
                response.setPaymentPath(result.getRequestPaymentResult().getPaymentPath());
                return response;

            case FAILED:
                logger.error("IPG Request init error: [{}]", result.getRequestPaymentResult().getResultStatus());
                throw new IPGException("IPG_STEP_ONE_" + IPGExceptionCodes.FAILED);

            case READY:
                logger.error("IPG Request init error: [{}]", result.getRequestPaymentResult().getResultStatus());
                throw new IPGException("IPG_STEP_ONE_" + IPGExceptionCodes.NOTHING_TO_DO);

            case USER_NOT_ACTIVE:
                logger.error("IPG Request init error: [{}]", result.getRequestPaymentResult().getResultStatus());
                throw new IPGException("IPG_STEP_ONE_" + IPGExceptionCodes.MERCHANT_IS_INACTIVE);

            case GATEWAY_UNVERIFY:
                logger.error("IPG Request init error: [{}]", result.getRequestPaymentResult().getResultStatus());
                throw new IPGException("IPG_STEP_ONE_" + IPGExceptionCodes.GATEWAY_IS_INACTIVE);

            case INVALID_SERVER_IP:
                logger.error("IPG Request init error: [{}]", result.getRequestPaymentResult().getResultStatus());
                throw new IPGException("IPG_STEP_ONE_" + IPGExceptionCodes.INVALID_SERVER_IP);

            case GATEWAY_IS_BLOCKED:
                logger.error("IPG Request init error: [{}]", result.getRequestPaymentResult().getResultStatus());
                throw new IPGException("IPG_STEP_ONE_" + IPGExceptionCodes.GATEWAY_IS_BLOCKED);

            case GATEWAY_IS_EXPIRED:
                logger.error("IPG Request init error: [{}]", result.getRequestPaymentResult().getResultStatus());
                throw new IPGException("IPG_STEP_ONE_" + IPGExceptionCodes.GATEWAY_IS_EXPIRED);

            case GATEWAY_INVALID_INFO:
                logger.error("IPG Request init error: [{}]", result.getRequestPaymentResult().getResultStatus());
                throw new IPGException("IPG_STEP_ONE_" + IPGExceptionCodes.INVALID_CREDENTIAL);

            default:
                logger.error("IPG Request init error: [{}]", result.getRequestPaymentResult().getResultStatus());
                throw new IPGException("IPG_STEP_ONE_" + IPGExceptionCodes.UNKNOWN);
        }

    }

    @Override
    public IPGStepTwoResponse verify(IPGStepTwoRequest request) throws IPGException {

        VerifyPayment verifyPayment = new ObjectFactory().createVerifyPayment();
        verifyPayment.setMerchantID(username);
        verifyPayment.setPassword(password);
        verifyPayment.setPrice(new BigDecimal(request.getPrice()));
        verifyPayment.setRefNum(request.getIPGReference());

        VerifyPaymentResponse result = (VerifyPaymentResponse)
                webServiceTemplate.marshalSendAndReceive(verifyPayment,
                        new WebServiceMessageCallback() {
                            @Override
                            public void doWithMessage(WebServiceMessage webServiceMessage) throws IOException, TransformerException {
                                ((SoapMessage) webServiceMessage).setSoapAction("http://tempuri.org/verifyPayment");
                            }
                        });


        switch (result.getVerifyPaymentResult().getResultStatus()) {
            case SUCCESS:
                IPGStepTwoResponse response = new IPGStepTwoResponse();
                response.setAmount(result.getVerifyPaymentResult().getPayementedPrice().longValue());
                return response;

            case READY:
                logger.error("IPG Verification error: [{}]", result.getVerifyPaymentResult().getResultStatus());
                throw new IPGException("IPG_STEP_TWO_" + IPGExceptionCodes.NOTHING_TO_DO);

            case NOT_MATCH_MONEY:
                logger.error("IPG Verification error: [{}]", result.getVerifyPaymentResult().getResultStatus());
                throw new IPGException("IPG_STEP_TWO_" + IPGExceptionCodes.AMOUNT_MISMATCH);

            case VERIFYED:
                logger.error("IPG Verification error: [{}]", result.getVerifyPaymentResult().getResultStatus());
                throw new IPGException("IPG_STEP_TWO_" + IPGExceptionCodes.DUPLICATE_TRANSACTION);

            case INVALID_REF:
                logger.error("IPG Verification error: [{}]", result.getVerifyPaymentResult().getResultStatus());
                throw new IPGException("IPG_STEP_TWO_" + IPGExceptionCodes.INVALID_REFERENCE);

            default:
                logger.error("IPG Verification error: [{}]", result.getVerifyPaymentResult().getResultStatus());
                throw new IPGException("IPG_STEP_TWO_" + IPGExceptionCodes.UNKNOWN);
        }
    }
}