package com.sollasi.base.enumtypes;

public enum UserStatus {
    ACTIVE,
    DEACTIVATED,
    NOT_COMPLETED,
    NOT_CHECKED,
    REJECTED
}
