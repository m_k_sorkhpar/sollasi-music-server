package com.sollasi.base.advicers;

import com.sollasi.base.exceptions.badrequest.FieldValidationBadRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@ControllerAdvice
@Slf4j
public class MethodArgumentNotValidHandler {

    private MessageSource messageSource;

    private final static Locale faLocale = new Locale("fa", "IR");

    @Autowired
    public MethodArgumentNotValidHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<List<FieldValidationBadRequest>> handleException(MethodArgumentNotValidException exception) {
        List<FieldValidationBadRequest> errors =
                exception.getBindingResult()
                        .getFieldErrors().stream().map(m -> {
                            String enMessage = m.getDefaultMessage();
                            String faMessage = "";

                            try {
                                enMessage = messageSource.getMessage(m.getDefaultMessage(), m.getArguments(), Locale.US);
                            } catch (NoSuchMessageException ex) {
                                logger.error("not found message key in Validation_en_US.properties Properties for key {}",
                                        m.getDefaultMessage());
                            }
                            try {
                                faMessage = messageSource.getMessage(m.getDefaultMessage(), m.getArguments(), faLocale);
                            } catch (NoSuchMessageException ex) {
                                logger.error("not found message key in Validation_fa_IR.properties Properties for key {}",
                                        m.getDefaultMessage());
                            }
                            return new FieldValidationBadRequest(m.getField(), enMessage, faMessage);
                        }
                ).collect(Collectors.toList());
        return ResponseEntity.badRequest().body(errors);
    }
}
