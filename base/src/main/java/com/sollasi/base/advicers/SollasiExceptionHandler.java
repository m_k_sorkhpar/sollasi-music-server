package com.sollasi.base.advicers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;

import java.util.HashMap;

@ControllerAdvice
@Slf4j
public class SollasiExceptionHandler {

    public static final String ACTIVATION_CODE_NOT_FOUND = "ACTIVATION_CODE_NOT_FOUND";
    public static final String ACTIVATION_ALREADY_USED = "ACTIVATION_ALREADY_USED";
    public static final String ACTIVATION_CODE_NOT_VALID = "ACTIVATION_CODE_NOT_VALID";
    public static final String CUSTOMER_NOT_FOUND = "CUSTOMER_NOT_FOUND";
    public static final String ALBUM_NOT_FOUND = "ALBUM_NOT_FOUND";
    public static final String ALBUM_NOT_ENOUGH_CREDIT = "ALBUM_NOT_ENOUGH_CREDIT";
    public static final String TRACK_NOT_FOUND = "TRACK_NOT_FOUND";
    public static final String TRACK_NOT_ENOUGH_CREDIT = "TRACK_NOT_ENOUGH_CREDIT";
    public static final String MEDIA_NOT_FOUND = "MEDIA_NOT_FOUND";
    public static final String ARTIST_NOT_FOUND = "ARTIST_NOT_FOUND";
    public static final String PLAYLIST_NOT_FOUND = "PLAYLIST_NOT_FOUND";
    public static final String CATEGORY_NOT_FOUND = "CATEGORY_NOT_FOUND";
    public static final String TRACK_NOT_PURCHASED = "TRACK_NOT_PURCHASED";
    public static final String NEWS_NOT_FOUND = "NEWS_NOT_FOUND";
    public static final String TRANSACTION_NOT_FOUND = "TRANSACTION_NOT_FOUND";
    public static final String DUPLICATE_CALLBACK = "DUPLICATE_CALLBACK";
    public static final String INVALID_AMOUNT = "INVALID_AMOUNT";
    public static final String PLAY_LIST_IS_NOT_BELONGS_TO_CUSTOMER = "PLAY_LIST_IS_NOT_BELONGS_TO_CUSTOMER";

    private static HashMap<String, String> errors = new HashMap<String, String>() {{
        put(ACTIVATION_CODE_NOT_FOUND, "برای این شماره کد فعالسازی ثبت نگردیده است");
        put(ACTIVATION_ALREADY_USED, "کد فعالسازی استفاده شده است");
        put(ACTIVATION_CODE_NOT_VALID, "کد فعاسازی اشتباه است");
        put(CUSTOMER_NOT_FOUND, "کاربر یافت نشد");
        put(ALBUM_NOT_FOUND, "آلبوم یافت نشد");
        put(ALBUM_NOT_ENOUGH_CREDIT, "اعتبار شما برای خرید این آلبوم کافی نیست");
        put(TRACK_NOT_FOUND, "آهنگ یافت نشد");
        put(TRACK_NOT_ENOUGH_CREDIT, "اعتبار شما برای خرید این آهنگ کافی نیست");
        put(MEDIA_NOT_FOUND, "فایل مورد نظر یافت نشد");
        put(ARTIST_NOT_FOUND, "خواننده یافت نشد");
        put(PLAYLIST_NOT_FOUND, "پلی لیست یافت نشد");
        put(CATEGORY_NOT_FOUND, "سبک موسیقی یافت نشد");
        put(TRACK_NOT_PURCHASED, "آهنگ مورد نظر خریداری نشده است");
        put(NEWS_NOT_FOUND, "خبر یافت نشد");
        put(TRANSACTION_NOT_FOUND, "تراکنش یافت نشد");
        put(INVALID_AMOUNT, "مبلغ درخواستی معتبر نیست");
        put(PLAY_LIST_IS_NOT_BELONGS_TO_CUSTOMER, "پلی لیست متعلق به کاربر دیگری است");
    }};

}