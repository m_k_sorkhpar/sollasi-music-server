package com.sollasi.base.exceptions.badrequest;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FieldValidationBadRequest {
    private String fieldName;
    private String enMessage;
    private String faMessage;
}
