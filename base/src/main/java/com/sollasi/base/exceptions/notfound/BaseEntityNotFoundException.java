package com.sollasi.base.exceptions.notfound;

public abstract class BaseEntityNotFoundException extends Exception {

    private Class<? extends BaseEntityNotFoundException> clazz;
    private Long id;

    protected BaseEntityNotFoundException(Class<? extends BaseEntityNotFoundException> clazz, Long id) {
        this.clazz = clazz;
        this.id = id;
    }
}
