package com.sollasi.base.conf.listeners;

import com.mysql.jdbc.AbandonedConnectionCleanupThread;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
@Slf4j
public class ApplicationShutdownListener implements ApplicationListener<ContextClosedEvent> {

  /*  @Autowired
    private RabbitTemplate rabbitTemplate;*/

    private ApplicationContext applicationContext;

    @Autowired
    public ApplicationShutdownListener(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public void onApplicationEvent(final ContextClosedEvent event) {
        logger.info("ServletContext destroyed");

        Enumeration<Driver> drivers = DriverManager.getDrivers();
        Driver d = null;
        while (drivers.hasMoreElements()) {
            try {
                d = drivers.nextElement();
                DriverManager.deregisterDriver(d);
                logger.info(String.format("Driver %s deregistered", d));
            } catch (SQLException ex) {
                logger.info(String.format("Error deregistering driver %s", d));
                ex.printStackTrace();
            }
        }

        AbandonedConnectionCleanupThread.checkedShutdown();

        Map<String, ThreadPoolTaskScheduler> schedulers = applicationContext.getBeansOfType(ThreadPoolTaskScheduler.class);

        for (ThreadPoolTaskScheduler scheduler : schedulers.values()) {
            scheduler.getScheduledExecutor().shutdown();
            try {
                scheduler.getScheduledExecutor().awaitTermination(20000, TimeUnit.MILLISECONDS);
                if (scheduler.getScheduledExecutor().isTerminated() || scheduler.getScheduledExecutor().isShutdown())
                    logger.info("Scheduler " + scheduler.getThreadNamePrefix() + " has stopped");
                else {
                    logger.info("Scheduler " + scheduler.getThreadNamePrefix() + " has not stopped normally " +
                            "and will be shut down immediately");
                    scheduler.getScheduledExecutor().shutdownNow();
                    logger.info("Scheduler " + scheduler.getThreadNamePrefix() + " has shut down immediately");
                }
            } catch (IllegalStateException | InterruptedException e) {
                e.printStackTrace();
            }
        }

        Map<String, ThreadPoolTaskExecutor> executors = applicationContext.getBeansOfType(ThreadPoolTaskExecutor.class);

        for (ThreadPoolTaskExecutor executor : executors.values()) {
            int retryCount = 0;
            while (executor.getActiveCount() > 0 && ++retryCount < 51) {
                try {
                    logger.info("Executor " + executor.getThreadNamePrefix() + " is still working with active "
                            + executor.getActiveCount() + " work. Retry count is " + retryCount);
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (!(retryCount < 51))
                logger.info("Executor " + executor.getThreadNamePrefix() + " is still working.Since Retry " +
                        "count exceeded max value " + retryCount + ", will be killed immediately");
            executor.shutdown();
            logger.info("Executor " + executor.getThreadNamePrefix() + " with active "
                    + executor.getActiveCount() + " work has killed");
        }

        LogManager.shutdown();
    }
}