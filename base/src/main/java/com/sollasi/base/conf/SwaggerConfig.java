package com.sollasi.base.conf;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@Configuration
@EnableSwagger2
@Import({springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration.class})
public class SwaggerConfig {
/*    @Autowired
    private TypeResolver typeResolver;*/

    @Bean
    public Docket ertebatFardaCentersApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.regex("/api/.*"))
                .build()
                .useDefaultResponseMessages(false)
                .securitySchemes(Lists.newArrayList(apiKey()));
    }

  /*  @Bean
    SecurityConfiguration security() {
        return new SecurityConfiguration(
                "test-app-client-id",
                "test-app-client-secret",
                "test-app-realm",
                "test-app",
                "Bearer access_token",
                ApiKeyVehicle.HEADER,
                "Authorization",
                "," *//*scope separator*//*);
    }*/

    @Bean
    SecurityScheme apiKey() {
        return new ApiKey("JWT", "Authorization", "header");
    }


    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Sol La Si server REST api")
                .description("Here you can find provided REST methods usages and descriptions. ")
                .contact(new Contact(
                        "sollasi Co.",
                        "sollasi.com",
                        "info@sollasi.com"))
                .version("1.0")
                .build();
    }
}
