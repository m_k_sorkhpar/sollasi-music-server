package com.sollasi.base.conf;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import java.util.Locale;


@Configuration
public class LocaleConfig {

    @Value("${com.sollasi.defaultLocale.language}")
    private String defaultLang;

    @Value("${com.sollasi.defaultLocale.country}")
    private String defaultCountry;

    @Bean
    public Locale defaultLocale() {
        return new Locale(defaultLang, defaultCountry);
    }

    @Bean
    public LocaleResolver localeResolver(Locale defaultLocale) {
        SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(defaultLocale);
        return slr;
    }

    @Bean
    public static MessageSource messageSource() {
        ResourceBundleMessageSource source = new ResourceBundleMessageSource();
        source.setBasenames("i18n/messages","i18n/ValidationMessages");  // name of the resource bundle
        source.setUseCodeAsDefaultMessage(false);
        source.setDefaultEncoding("UTF-8");
        return source;
    }
}
