package com.sollasi.base.util;

import java.util.stream.IntStream;

public class CheckingUtil {
    public static final String IRAN_EN_PHONE_NUMBER =
            "(\\+989|9|09)" +
                    "(" +
                    "0[1-3]|" +
                    "1[0-9]|" +
                    "2[0-2]|" +
                    "3[0-9]|" +
                    "90|" +
                    "9[8-9]|" +
                    ")" +
                    "\\d{7}";
    public static final String IRAN_FA_PHONE_NUMBER =
            "(\\+\\u0669\\u0668\\u0669|" +//+98
                    "\\+\\u06F9\\u06F8\\u06F9|" +//+098
                    "\\u0669|" +//9
                    "\\u06F9|" +//9
                    "\\u06F0\\u06F9|" +//09
                    "\\u0660\\u0669)" +//09
                    "(" +
                    "\\u0660[\\u0661-\\u0663]|" +//0[1-3]
                    "\\u06F0[\\u06F1-\\u06F3]|" +//0[1-3]
                    "\\u0661[\\u0660-\\u0669]|" +//1[0-9]
                    "\\u06F1[\\u06F0-\\u06F9]|" +//1[0-9]
                    "\\u0662[\\u0660-\\u0662]|" +//2[0-2]
                    "\\u06F2[\\u06F0-\\u06F2]|" +//2[0-2]
                    "\\u0663[\\u0660-\\u0669]|" +//3[0-9]
                    "\\u06F3[\\u06F0-\\u06F9]|" +//3[0-9]
                    "\\u0669\\u0660|" +//90
                    "\\u06F9\\u06F0|" +//90
                    "\\u0669[\\u0668-\\u0669]|" +//9[8-9]
                    "\\u06F9[\\u06F8-\\u06F9]|" +//9[8-9]
                    ")" +
                    "[\\u0660-\\u0669\\u06F0-\\u06F9]{7}";//[0-9]{7}

    public static final String EN_ALPHABETIC = "^[a-zA-Z]*$";
    public static final String FA_ALPHABETIC = "^[\\u06A9\\u06AF\\u06C0\\u06CC\\u060C\\u062A\\u062B\\u062C\\u062D\\u062E\\u062F\\u063A\\u064B\\u064C\\u064D\\u064E\\u064F\\u067E\\u0670\\u0686\\u0698\\u200C\\u06A9\\u06A9\\u0621-\\u0629\\u0630-\\u0639\\u0641-\\u0654]*$";

    public static final String EN_DIGITS = "^[0-9]*$";
    public static final String FA_DIGITS = "^[\\u0660-\\u0669\\u06F0-\\u06F9]*$";

    public static final String EN_ALPHA_NUMERIC = "^[a-zA-Z0-9]*$";
    public static final String FA_ALPHA_NUMERIC = "^[\\u0660-\\u0669\\u06F0-\\u06F9\\u06A9\\u06AF\\u06C0\\u06CC\\u060C\\u062A\\u062B\\u062C\\u062D\\u062E\\u062F\\u063A\\u064B\\u064C\\u064D\\u064E\\u064F\\u067E\\u0670\\u0686\\u0698\\u200C\\u06A9\\u06A9\\u0621-\\u0629\\u0630-\\u0639\\u0641-\\u0654]*$";


    public static final String EN_ALPHA_NUMERIC_WITH_SPACE = "^[\\sa-zA-Z0-9]*$";
    public static final String FA_ALPHA_NUMERIC_WITH_SPACE = "^[\\s\\u0660-\\u0669\\u06F0-\\u06F9\\u06A9\\u06AF\\u06C0\\u06CC\\u060C\\u062A\\u062B\\u062C\\u062D\\u062E\\u062F\\u063A\\u064B\\u064C\\u064D\\u064E\\u064F\\u067E\\u0670\\u0686\\u0698\\u200C\\u06A9\\u06A9\\u0621-\\u0629\\u0630-\\u0639\\u0641-\\u0654]*$";


    public static final String THEN_DIGITS = "^\\p{N}{10}$";
    public static final String IP_ADDRESS = "^((?:(?:25[0-5]2[0-4][0-9][01]?[0-9][0-9]?)\\.){3}(?:25[0-5]2[0-4][0-9][01]?[0-9][0-9]?))*$";
    public static final String EMAIL = "^([a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4})*$";
    public static final String URL = "\b(https?ftpfileldap)://[-A-Za-z0-9+&@#/%?=~_!:,.;]*[-A-Za-z0-9+&@#/%=~_]";
    //Strong password => min 6 char => lowercase + one uppercase + on number
    public static final String STRONG_PASSWORD = "^(?=^.{6,}$)((?=.*[A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z]))^.*$";


    public static boolean isValidIranianNationalCode(String input) {
        if (!input.matches("^\\p{N}{10}$"))
            return false;

        int check = Integer.parseInt(input.substring(9, 10));

        int sum = IntStream.range(0, 9)
                .map(x ->
                        Integer.parseInt(input.substring(x, x + 1)) * (10 - x))
                .sum() % 11;

        return (sum < 2 && check == sum) || (sum >= 2 && check + sum == 11);
    }

    public static String formatIranianPhone(String phone) throws Exception {
        StringBuilder format = new StringBuilder("+98");
        boolean isValidEN = phone.matches(IRAN_EN_PHONE_NUMBER);
        boolean isValidFA = phone.matches(IRAN_FA_PHONE_NUMBER);

        if (!isValidEN && !isValidFA)
            throw new Exception("phone number not valid");

        if (phone.length() == 13) {
            phone = phone.substring(3);
        } else if (phone.length() == 11) {
            phone = phone.substring(1);
        } else if (phone.length() != 10)
            return null;

        if (isValidEN) {
            format.append(phone);
        } else {
            for (int i = 0; i < 10; i++) {
                format.append(
                        Integer.parseInt(
                                phone.substring(i, i + 1))
                );
            }
        }
        return format.toString();
    }
}
