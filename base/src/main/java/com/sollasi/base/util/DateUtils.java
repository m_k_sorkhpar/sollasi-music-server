package com.sollasi.base.util;

import com.ibm.icu.util.Calendar;
import org.apache.commons.lang.StringUtils;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by pc on 9/7/2017.
 */
public class DateUtils {
    private static final SimpleDateFormat sdfExactTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");

    private static final String persianDateTimeRegexFormat = "([\\d]{4})\\/([0-1][\\d])\\/([0-3][\\d])\\s([0-2][\\d]):([0-5][\\d]):([0-5][\\d])";

    //1396/06/16 10:17:33
    public static String toPersianDateTime(Date date) {
        if (date == null) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        JalaliCalendar.YearMonthDate pc = JalaliCalendar.gregorianToJalali(new JalaliCalendar.YearMonthDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE)));
        return StringUtils.leftPad(String.valueOf(pc.getYear()), 2, '0')
                + "/" + StringUtils.leftPad(String.valueOf(pc.getMonth() + 1), 2, '0')
                + "/" + StringUtils.leftPad(String.valueOf(pc.getDate()), 2, '0')
                + " " + sdfExactTime.format(date);

    }

    public static String toPersianDateTime(LocalDate date) {
        if (date == null) {
            return null;
        }
        JalaliCalendar.YearMonthDate pc =
                JalaliCalendar.gregorianToJalali(
                        new JalaliCalendar.YearMonthDate(date.getYear(),
                                date.getMonthValue(),
                                date.getDayOfMonth()));

        return StringUtils.leftPad(String.valueOf(pc.getYear()), 2, '0')
                + "/" + StringUtils.leftPad(String.valueOf(pc.getMonth() + 1), 2, '0')
                + "/" + StringUtils.leftPad(String.valueOf(pc.getDate()), 2, '0');
    }

    public static String toPersianDateTime(LocalDateTime dateTime) {
        if (dateTime == null) {
            return null;
        }
        JalaliCalendar.YearMonthDate pc =
                JalaliCalendar.gregorianToJalali(
                        new JalaliCalendar.YearMonthDate(dateTime.getYear(),
                                dateTime.getMonthValue(),
                                dateTime.getDayOfMonth()));

        return StringUtils.leftPad(String.valueOf(pc.getYear()), 2, '0')
                + "/" + StringUtils.leftPad(String.valueOf(pc.getMonth() + 1), 2, '0')
                + "/" + StringUtils.leftPad(String.valueOf(pc.getDate()), 2, '0')
                + " " + dateTime.format(dateTimeFormatter);
    }

    public static boolean isPersianDateTime(String persianDateTime) {
        Pattern pattern = Pattern.compile(persianDateTimeRegexFormat);
        Matcher matcher = pattern.matcher(persianDateTime);
        return matcher.find();
    }

    public static Date fromPersianDateTime(String persianDateTime) {
        Pattern pattern = Pattern.compile(persianDateTimeRegexFormat);
        Matcher matcher = pattern.matcher(persianDateTime);
        if (matcher.find()) {
            int year = Integer.parseInt(matcher.group(1));
            int month = Integer.parseInt(matcher.group(2));
            int date = Integer.parseInt(matcher.group(3));
            int hours = Integer.parseInt(matcher.group(4));
            int minutes = Integer.parseInt(matcher.group(5));
            int seconds = Integer.parseInt(matcher.group(6));

            JalaliCalendar.YearMonthDate pc = JalaliCalendar.jalaliToGregorian(new JalaliCalendar.YearMonthDate(year, month - 1, date));
            Calendar cal = Calendar.getInstance();
            cal.set(pc.getYear(), pc.getMonth(), pc.getDate(), hours, minutes, seconds);
            return cal.getTime();
        } else {
            return null;
        }
    }

    /**
     * @param date1
     * @param date2
     * @return Difference of two dates in Milli seconds. if date1.time > date2.time the returned value is positive, it returns negative elsewhere
     */
    public static Long getDatesDiffInMilliSeconds(Date date1, Date date2) {
        assert date1 != null;
        assert date2 != null;
        return date1.getTime() - date2.getTime();
    }

    /**
     * @param date1
     * @param date2
     * @return Difference of two dates in Seconds. if date1.time > date2.time the returned value is positive, it returns negative elsewhere
     */
    public static Long getDatesDiffInSeconds(Date date1, Date date2) {
        Long milliSecondsDiff = getDatesDiffInMilliSeconds(date1, date2);
        return milliSecondsDiff / 1000;
    }

    /**
     * @param date1
     * @param date2
     * @return Difference of two dates in Minutes. if date1.time > date2.time the returned value is positive, it returns negative elsewhere
     */
    public static Long getDatesDiffInMinutes(Date date1, Date date2) {
        Long secondsDiff = getDatesDiffInSeconds(date1, date2);
        return secondsDiff / 60;
    }


}
