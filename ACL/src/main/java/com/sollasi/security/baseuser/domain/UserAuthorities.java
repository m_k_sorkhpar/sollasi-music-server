package com.sollasi.security.baseuser.domain;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

@Entity
@Table(name = "users_authorities")
@Data
@Accessors
public class UserAuthorities implements GrantedAuthority {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Column(name = "authority", unique = true)
    private String authority;

    @Override
    public String getAuthority() {
        return authority;
    }
}
