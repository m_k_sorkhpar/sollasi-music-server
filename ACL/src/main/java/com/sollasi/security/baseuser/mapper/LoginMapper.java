package com.sollasi.security.baseuser.mapper;

import com.sollasi.security.baseuser.domain.BaseUser;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface LoginMapper {
    LoginResponse baseUserToLoginResponse(String token);
}
