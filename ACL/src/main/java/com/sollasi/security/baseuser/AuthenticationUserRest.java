package com.sollasi.security.baseuser;

import com.sollasi.security.baseuser.facade.LoginFacade;
import com.sollasi.security.baseuser.mapper.ActivationRequest;
import com.sollasi.security.baseuser.mapper.LoginResponse;
import com.sollasi.security.baseuser.mapper.ValidateUserRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;


@RestController
@RequestMapping("/api/auth")
public class AuthenticationUserRest {


    private final AuthenticationManager authenticationManager;
    private LoginFacade loginFacade;

    @Autowired
    public AuthenticationUserRest(AuthenticationManager authenticationManager, LoginFacade loginFacade) {
        this.authenticationManager = authenticationManager;
        this.loginFacade = loginFacade;
    }

/*
    @PostMapping("/login")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success", response = LoginResponse.class)
            , @ApiResponse(code = 400, message = "bad parameter (JsonArray)",response = FieldValidationBadRequest.class)
    })
    public ResponseEntity<LoginResponse> authorize(@Valid @RequestBody LoginRequest loginRequest) {

        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword());

        Authentication authentication = authenticationManager.authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        return loginFacade.login(authentication, loginRequest);
    }
*/

    @PostMapping("/activationCode")
    public ResponseEntity getActivationCode(@Valid @RequestBody ActivationRequest request) throws Exception {
        loginFacade.sendActivationCode(request.getPhoneNumber());
        return ResponseEntity.ok().build();
    }

    @PostMapping("/validate")
    public ResponseEntity<LoginResponse> validate(@Valid @RequestBody ValidateUserRequest request,
                                                  HttpServletRequest servletRequest) throws Exception {
        LoginResponse response = loginFacade.validateUserByActivationCode(request.getPhoneNumber(),
                request.getActivationCode(), request.getDeviceId(), servletRequest.getRemoteAddr()
        );
        return ResponseEntity.ok(response);
    }

}
