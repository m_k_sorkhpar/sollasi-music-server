package com.sollasi.security.baseuser.repo;

import com.sollasi.security.baseuser.domain.ActivationCode;
import com.sollasi.security.baseuser.domain.BaseUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ActivationCodeRepository extends JpaRepository<ActivationCode,Long>{

    ActivationCode findTopByPhoneNumberOrderByCreationDatetimeDesc(@Param("phoneNumber") String phoneNumber);
}
