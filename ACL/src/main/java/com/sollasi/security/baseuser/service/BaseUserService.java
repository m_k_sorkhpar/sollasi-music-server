package com.sollasi.security.baseuser.service;

import com.sollasi.security.baseuser.domain.BaseUser;
import com.sollasi.security.baseuser.repo.BaseUserRepo;
import com.sollasi.security.baseuser.repo.CustomerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class BaseUserService implements UserDetailsService {

    private BaseUserRepo userRepository;

    @Autowired
    public BaseUserService(BaseUserRepo repo) {
        this.userRepository = repo;
    }


    @Override
    public UserDetails loadUserByUsername(final String username) {
        logger.debug("Authenticating {}", username);
        String lowercaseLogin = username.toLowerCase(Locale.ENGLISH);
        Optional<BaseUser> userByEmailFromDatabase = userRepository.findOneWithAuthoritiesByEmail(lowercaseLogin);
        return userByEmailFromDatabase.map(this::createSpringSecurityUser).orElseGet(() -> {
            Optional<BaseUser> userByLoginFromDatabase = userRepository.findOneWithAuthoritiesByUsername(lowercaseLogin);
            return userByLoginFromDatabase.map(this::createSpringSecurityUser)
                    .orElseThrow(() -> new UsernameNotFoundException("User " + lowercaseLogin + " was not found in the " +
                            "database"));
        });
    }

    private org.springframework.security.core.userdetails.User createSpringSecurityUser(BaseUser user) {
        List<GrantedAuthority> grantedAuthorities = user.getAuthorities().stream()
                .map(authority -> new SimpleGrantedAuthority(authority.getAuthority()))
                .collect(Collectors.toList());
        return new org.springframework.security.core.userdetails.User(user.getUsername(),
                user.getPassword(),
                grantedAuthorities);
    }

    public BaseUser loadByUsername(final String username) {
        return userRepository.findByUsername(username);
    }

    public void registerUser(BaseUser user) {
        userRepository.save(user);
    }
}
