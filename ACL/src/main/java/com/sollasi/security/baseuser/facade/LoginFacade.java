package com.sollasi.security.baseuser.facade;

import com.kavenegar.sdk.KavenegarApi;
import com.sollasi.base.advicers.SollasiExceptionHandler;
import com.sollasi.base.enumtypes.UserStatus;
import com.sollasi.base.util.CheckingUtil;
import com.sollasi.security.baseuser.domain.ActivationCode;
import com.sollasi.security.baseuser.domain.BaseUser;
import com.sollasi.security.baseuser.domain.Customer;
import com.sollasi.security.baseuser.domain.UserAuthorities;
import com.sollasi.security.baseuser.mapper.CustomerProfileResponse;
import com.sollasi.security.baseuser.mapper.LoginMapper;
import com.sollasi.security.baseuser.mapper.LoginResponse;
import com.sollasi.security.baseuser.repo.ActivationCodeRepository;
import com.sollasi.security.baseuser.repo.UserAuthoritiesRepo;
import com.sollasi.security.baseuser.service.BaseUserService;
import com.sollasi.security.util.TokenProvider;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Random;

@Service
@Slf4j
public class LoginFacade {
    private BaseUserService userService;
    private final TokenProvider tokenProvider;
    private ActivationCodeRepository activationCodeRepository;
    private UserAuthoritiesRepo authoritiesRepo;
    private LoginMapper mapper;
    private KavenegarApi kavenegarApi;
    @Value("${com.sollasi.sms.apiKey}")
    private String apiKey;

    @PostConstruct
    public void init() {
        this.kavenegarApi = new KavenegarApi(apiKey);
    }

    @Autowired
    public LoginFacade(BaseUserService userService, ActivationCodeRepository activationCodeRepository,
                       UserAuthoritiesRepo authoritiesRepo,
                       TokenProvider tokenProvider, LoginMapper mapper) {
        this.userService = userService;
        this.activationCodeRepository = activationCodeRepository;
        this.tokenProvider = tokenProvider;
        this.authoritiesRepo = authoritiesRepo;
        this.mapper = mapper;
    }

    /*@Transactional(readOnly = true)
    public LoginResponse login(Authentication authentication, LoginRequest loginRequest) {
        boolean rememberMe = (loginRequest.getRememberMe() == null) ? false : loginRequest.getRememberMe();
        String jwt = tokenProvider.createToken(authentication, rememberMe);
        return new LoginResponse(jwt);
    }
*/
    public void sendActivationCode(String phoneNumber) throws Exception {
        String code = String.valueOf(10001 + new Random().nextInt(89998));
        phoneNumber = CheckingUtil.formatIranianPhone(phoneNumber);
        assert phoneNumber != null;
        ActivationCode activationCode = new ActivationCode();
        activationCode.setActivationCode(code);
        activationCode.setPhoneNumber(phoneNumber);
        try {
            kavenegarApi.verifyLookup(phoneNumber.replace("\\+98", "0"), code, "activation");
            activationCodeRepository.save(activationCode);
        } catch (Exception e) {
            logger.error("sending activationCode failed", e);
            throw e;
        }
    }


    public LoginResponse validateUserByActivationCode(String phoneNumber, String activationCode, String deviceId, String ip)
            throws Exception {
        logger.info("trying to activate user [phoneNumber: " + phoneNumber + ", activationCode: " + activationCode +
                ", deviceId: " + deviceId + ", ip: " + ip + "]"
        );
        assert !StringUtils.isEmpty(phoneNumber);
        assert !StringUtils.isEmpty(activationCode);
        String plainMobileNo = CheckingUtil.formatIranianPhone(phoneNumber);
        ActivationCode activationCodeEntity = activationCodeRepository.
                findTopByPhoneNumberOrderByCreationDatetimeDesc(plainMobileNo);
        if (activationCodeEntity == null) {
            logger.info("no activation code generated for user [phoneNumber: " + plainMobileNo + "]");
            throw new Exception(SollasiExceptionHandler.ACTIVATION_CODE_NOT_FOUND);
        }
        if (activationCodeEntity.isUsed()) {
            logger.info("activation code generated for user [phoneNumber: " + plainMobileNo + "] is already consumed");
            throw new Exception(SollasiExceptionHandler.ACTIVATION_ALREADY_USED);
        }
        if (!StringUtils.equals(activationCode, activationCodeEntity.getActivationCode())) {
            logger.info("activation code mismatch for user [phoneNumber: " + plainMobileNo + "]");
            throw new Exception(SollasiExceptionHandler.ACTIVATION_CODE_NOT_VALID);
        }
        activationCodeEntity.setUsed(true);
        activationCodeEntity.setIp(ip);
        activationCodeEntity.setDeviceId(deviceId);
        activationCodeRepository.save(activationCodeEntity);

        BaseUser user = userService.loadByUsername(plainMobileNo);
        if (user == null) {
            UserAuthorities customerAuthority = authoritiesRepo.findByAuthority("customer");
            user = new Customer();
            user.setUsername(plainMobileNo);
            user.setStatus(UserStatus.ACTIVE);
            user.authority(customerAuthority);
            userService.registerUser(user);
        }
        CustomerProfileResponse profileResponse = new CustomerProfileResponse();
        profileResponse.setUsername(user.getUsername());
        profileResponse.setName(user.getFirstName());
        profileResponse.setFamily(user.getLastName());
        profileResponse.setEmail(user.getEmail());
        if (user instanceof Customer) {
            Customer customer = (Customer) user;
            profileResponse.setCredit(customer.getCredit());
            if (customer.getVipExpiration() != null) {
                Duration duration = Duration.between(customer.getVipExpiration(), LocalDateTime.now());
                profileResponse.setDayUntilExpireVIP(duration.toDays());
            } else {
                profileResponse.setDayUntilExpireVIP(0);
            }
        }
        return new LoginResponse(tokenProvider.createSystemToken(user), profileResponse);
    }

}
