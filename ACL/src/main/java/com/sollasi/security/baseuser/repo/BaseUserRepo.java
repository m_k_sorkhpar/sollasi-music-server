package com.sollasi.security.baseuser.repo;

import com.sollasi.security.baseuser.domain.BaseUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BaseUserRepo extends JpaRepository<BaseUser,Long>{
    BaseUser findByUsername(String username);

    @EntityGraph(attributePaths = "authorities")
    Optional<BaseUser> findOneWithAuthoritiesByUsername(String username);

    @EntityGraph(attributePaths = "authorities")
    Optional<BaseUser> findOneWithAuthoritiesByEmail(String email);

}
