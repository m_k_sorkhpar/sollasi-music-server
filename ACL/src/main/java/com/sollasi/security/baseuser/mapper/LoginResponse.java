package com.sollasi.security.baseuser.mapper;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors
@ApiModel
@AllArgsConstructor
@NoArgsConstructor
public class LoginResponse {
    private String token;
    private CustomerProfileResponse profile;
}
