package com.sollasi.security.baseuser.mapper;


import lombok.Data;

@Data
public class ValidateUserRequest {
    private String phoneNumber;
    private String activationCode;
    private String deviceId;
}
