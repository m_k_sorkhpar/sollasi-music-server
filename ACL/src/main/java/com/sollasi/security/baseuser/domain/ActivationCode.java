package com.sollasi.security.baseuser.domain;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "activation_code")
@Data
public class ActivationCode {

    @Id
    @Column(name = "activation_code_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "phoneNumber")
    private String phoneNumber;

    @Column(name = "code")
    private String activationCode;

    @Column(name = "creation_datetime", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private LocalDateTime creationDatetime = LocalDateTime.now();

    @Column(name = "is_used",columnDefinition = "BOOLEAN DEFAULT FALSE")
    private boolean used;

    @Column(name = "ip")
    private String ip;

    @Column(name = "device_id")
    private String deviceId;
}
