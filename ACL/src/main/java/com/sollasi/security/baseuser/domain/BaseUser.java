package com.sollasi.security.baseuser.domain;

import com.sollasi.base.enumtypes.Gender;
import com.sollasi.base.enumtypes.UserStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "base_user")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "user_type")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors
public class BaseUser implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "username", length = 40, unique = true, nullable = false)
    private String username;

    @Column(name = "password", length = 70)
    private String password;

    @Column(name = "first_name", length = 100)
    private String firstName;

    @Column(name = "last_name", length = 80)
    private String lastName;

    @Column(name = "last_password_reset_date")
    private LocalDateTime lastPasswordResetDate;

    @Column(name = "email", length = 150)
    private String email;

    @Size(min = 2, max = 6)
    @Column(name = "lang_key", length = 6)
    private String langKey;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Enumerated(EnumType.STRING)
    private UserStatus status;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE})
    @JoinTable(name = "users_m2m_authorities", joinColumns = {
            @JoinColumn(name = "user_fk", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "user_authorities_fk",
                    nullable = false, updatable = false)})
    private List<UserAuthorities> authorities;

    public BaseUser authority(UserAuthorities userAuthority) {
        if (this.authorities == null) {
            this.authorities = new ArrayList<UserAuthorities>();
        }
        this.authorities.add(userAuthority);
        return this;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.status != null && this.status.equals(UserStatus.ACTIVE);
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.status != null && this.status.equals(UserStatus.ACTIVE);
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.status != null && this.status.equals(UserStatus.ACTIVE);
    }

    @Override
    public boolean isEnabled() {
        return this.status != null && this.status.equals(UserStatus.ACTIVE);
    }
}