package com.sollasi.security.baseuser.mapper;

import lombok.Data;

@Data
public class ActivationRequest {
    String phoneNumber;
}
