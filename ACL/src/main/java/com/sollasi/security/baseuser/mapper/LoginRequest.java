package com.sollasi.security.baseuser.mapper;

import com.sollasi.base.util.CheckingUtil;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@ApiModel
public class LoginRequest {

    @NotNull
    @Size(min = 1, max = 50, message = "errors.loginRequest.username.size")
    @Pattern(regexp = CheckingUtil.EN_ALPHA_NUMERIC)
    private String username;

    @NotNull
    @Size(min = 6, max = 100, message = "errors.loginRequest.password.size")
    private String password;

    private Boolean rememberMe;

}
