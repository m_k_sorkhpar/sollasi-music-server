package com.sollasi.security.baseuser.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.time.LocalDateTime;

@Entity
@DiscriminatorValue("customer")
@Data
@Accessors
public class Customer extends BaseUser {

    @Column(name = "credit", columnDefinition = "BIGINT(20) DEFAULT 0")
    private long credit;

    @Column(name = "vip_expiration_date")
    private LocalDateTime vipExpiration;

}

