package com.sollasi.security.baseuser.mapper;

import lombok.Data;

@Data
public class CustomerProfileResponse {

    private String name;

    private String family;

    private String username;

    private String email;

    private long credit;

    private long dayUntilExpireVIP;

}
