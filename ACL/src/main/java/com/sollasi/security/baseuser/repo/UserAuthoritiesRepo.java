package com.sollasi.security.baseuser.repo;

import com.sollasi.security.baseuser.domain.BaseUser;
import com.sollasi.security.baseuser.domain.UserAuthorities;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserAuthoritiesRepo extends JpaRepository<UserAuthorities,Long>{

    UserAuthorities findByAuthority(String authorityName);
}
