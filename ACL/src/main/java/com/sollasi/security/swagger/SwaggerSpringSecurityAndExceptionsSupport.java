package com.sollasi.security.swagger;

import com.fasterxml.classmate.TypeResolver;
import com.google.common.base.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import springfox.documentation.RequestHandler;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.OperationBuilderPlugin;
import springfox.documentation.spi.service.RequestHandlerProvider;
import springfox.documentation.spi.service.contexts.OperationContext;
import springfox.documentation.spi.service.contexts.RequestMappingContext;
import springfox.documentation.spring.web.WebMvcRequestHandler;
import springfox.documentation.spring.web.plugins.DocumentationPluginsManager;
import springfox.documentation.spring.web.readers.parameter.ModelAttributeParameterExpander;
import springfox.documentation.swagger.common.SwaggerPluginSupport;

import java.lang.reflect.Method;
import java.util.Collections;

@Component
@Order(SwaggerPluginSupport.SWAGGER_PLUGIN_ORDER + 1001)
@Slf4j
public class SwaggerSpringSecurityAndExceptionsSupport implements OperationBuilderPlugin {

    @Override
    public void apply(OperationContext context) {
        Optional<PreAuthorize> annotation =
                context.findAnnotation(PreAuthorize.class);
        if (annotation.isPresent()) {
            context.operationBuilder().notes(annotation.get().value());
        }

    }

    @Override
    public boolean supports(DocumentationType documentationType) {
        return true;
    }
}