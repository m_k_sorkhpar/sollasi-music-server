package com.sollasi.server.news.domain;

import com.sollasi.server.media.domain.Media;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "news")
@Data
@Accessors
public class News {

    @Id
    @Column(name = "news_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "content", columnDefinition = "TEXT")
    private String content;

    @Column(name = "title")
    private String title;

    @Column(name = "creation_datetime", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private LocalDateTime creationDatetime = LocalDateTime.now();

    @ManyToOne
    @JoinColumn(name = "image_fk")
    private Media image;

    @Column(name = "like_counter", columnDefinition = "BIGINT(20) DEFAULT 0")
    private Long likeCounter;

    @Column(name = "dislike_counter", columnDefinition = "BIGINT(20) DEFAULT 0")
    private long dislikeCounter;
}

