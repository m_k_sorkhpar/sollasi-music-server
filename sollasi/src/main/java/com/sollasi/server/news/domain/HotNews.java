package com.sollasi.server.news.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Entity
@Table(name = "hot_news")
@Data
@Accessors
public class HotNews {

    @Id
    @Column(name = "hot_news_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "news_fk")
    private News news;

    @Column(name = "priority", nullable = false, unique = true)
    private Integer priority;
}

