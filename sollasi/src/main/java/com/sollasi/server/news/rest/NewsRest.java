package com.sollasi.server.news.rest;

import com.sollasi.server.news.rest.response.NewsFeedbackResponse;
import com.sollasi.server.news.rest.response.NewsResponse;
import com.sollasi.server.news.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/api/news")
public class NewsRest {

    @Autowired
    private NewsService newsService;

    @GetMapping("/")
    public NewsResponse getNews(@RequestParam(value = "offset") int offset, @RequestParam(value = "limit") int limit) {
        return newsService.getNews(offset, limit);
    }

    @GetMapping("/hot")
    public NewsResponse getHotNews() {
        return newsService.getHotNews();
    }

    @PostMapping("/like/{newsId}")
    public NewsFeedbackResponse likeTrack(Principal principal, @PathVariable("newsId") Long newsId) throws Exception {
        return newsService.likeNews(principal.getName(), newsId);
    }

    @PostMapping("/dislike/{newsId}")
    public NewsFeedbackResponse dislikeTrack(Principal principal, @PathVariable("newsId") Long newsId) throws Exception {
        return newsService.dislikeNews(principal.getName(), newsId);
    }


}
