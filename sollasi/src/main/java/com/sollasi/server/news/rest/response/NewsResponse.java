package com.sollasi.server.news.rest.response;

import com.sollasi.base.util.DateUtils;
import com.sollasi.server.news.domain.News;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewsResponse {
    private long total;
    private List<NewsResponseElement> news;

    public void add(News newsEntity) {
        if (news == null) {
            news = new ArrayList<>();
        }
        news.add(new NewsResponseElement(newsEntity));
    }

    @Getter
    @Setter
    public class NewsResponseElement {
        private Long id;

        private String title;

        private String content;

        private String date;

        private Long mediaId;

        private long likeCounter;

        private long dislikeCounter;

        public NewsResponseElement(News news) {
            this.id = news.getId();
            this.title = news.getTitle();
            this.content = news.getContent();
            if (news.getCreationDatetime() != null) {
                this.date = DateUtils.toPersianDateTime(news.getCreationDatetime());
            }
            this.mediaId = news.getImage() != null ? news.getImage().getId() : null;
            this.likeCounter = news.getLikeCounter();
            this.dislikeCounter = news.getDislikeCounter();
        }
    }
}
