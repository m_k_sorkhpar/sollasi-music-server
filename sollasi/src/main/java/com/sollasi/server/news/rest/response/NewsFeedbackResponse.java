package com.sollasi.server.news.rest.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NewsFeedbackResponse {
    private Long totalLike;
    private Long totalDislike;
}
