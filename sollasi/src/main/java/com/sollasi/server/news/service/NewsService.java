package com.sollasi.server.news.service;

import com.sollasi.base.advicers.SollasiExceptionHandler;
import com.sollasi.base.util.GridPagination;
import com.sollasi.security.baseuser.domain.Customer;
import com.sollasi.server.news.domain.HotNews;
import com.sollasi.server.news.domain.News;
import com.sollasi.server.news.repository.HotNewsRepository;
import com.sollasi.server.news.repository.NewsRepository;
import com.sollasi.server.news.rest.response.NewsFeedbackResponse;
import com.sollasi.server.news.rest.response.NewsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NewsService {
    @Autowired
    private NewsRepository newsRepository;
    @Autowired
    private HotNewsRepository hotNewsRepository;

    public News getNewsEntity(Long newsId) throws Exception {
        News news = newsRepository.findOne(newsId);
        if (news == null) {
            throw new Exception(SollasiExceptionHandler.NEWS_NOT_FOUND);
        }
        return news;
    }

    public NewsResponse getNews(int offset, int limit) {
        Page<News> newsList = newsRepository.findAll(new GridPagination(offset, limit, Sort.Direction.DESC, "creationDatetime"));
        NewsResponse response = new NewsResponse();
        for (News news : newsList.getContent()) {
            response.add(news);
        }
        response.setTotal(newsList.getTotalElements());
        return response;
    }

    public NewsResponse getHotNews() {
        List<HotNews> hotNewsList = hotNewsRepository.findAll(new Sort(Sort.Direction.DESC, "priority"));
        NewsResponse response = new NewsResponse();
        for (HotNews hotNews : hotNewsList) {
            response.add(hotNews.getNews());
        }
        return response;
    }

    public NewsFeedbackResponse likeNews(String username, Long newsId) throws Exception {
        News news = getNewsEntity(newsId);
        news.setLikeCounter(news.getLikeCounter() + 1);
        newsRepository.save(news);
        return new NewsFeedbackResponse(news.getLikeCounter(), news.getDislikeCounter());
    }

    public NewsFeedbackResponse dislikeNews(String username, Long newsId) throws Exception {
        News news = getNewsEntity(newsId);
        news.setDislikeCounter(news.getDislikeCounter() + 1);
        newsRepository.save(news);
        return new NewsFeedbackResponse(news.getLikeCounter(), news.getDislikeCounter());
    }

    public NewsResponse searchNews(Customer customer, String word) {
        return null;
    }
}
