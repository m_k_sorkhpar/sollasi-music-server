package com.sollasi.server.news.repository;

import com.sollasi.security.baseuser.domain.BaseUser;
import com.sollasi.server.news.domain.News;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NewsRepository extends JpaRepository<News, Long> {

}
