package com.sollasi.server.customer.repository;

import com.sollasi.security.baseuser.domain.Customer;
import com.sollasi.server.customer.domain.activities.TrackPurchased;
import com.sollasi.server.track.domain.Track;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrackPurchasedRepository extends JpaRepository<TrackPurchased, Long> {
    TrackPurchased findByCustomerAndTrack(Customer customer, Track track);
}
