package com.sollasi.server.customer.rest.request;

import lombok.Data;

@Data
public class IncreaseCreditRequest {
    private Long amount;
}
