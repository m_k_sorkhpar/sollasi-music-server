package com.sollasi.server.customer.repository;

import com.sollasi.security.baseuser.domain.Customer;
import com.sollasi.server.customer.domain.activities.TrackFeedback;
import com.sollasi.server.track.domain.Track;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrackFeedbackRepository extends JpaRepository<TrackFeedback, Long> {
    TrackFeedback findByCustomerAndTrack(Customer customer, Track track);
}
