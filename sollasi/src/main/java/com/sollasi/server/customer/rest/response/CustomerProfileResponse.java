package com.sollasi.server.customer.rest.response;

import com.sollasi.security.baseuser.domain.Customer;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Duration;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class CustomerProfileResponse {

    private String name;

    private String family;

    private String username;

    private String email;

    private long credit;

    private long dayUntilExpireVIP;

    public CustomerProfileResponse(Customer customer) {
        this.name = customer.getFirstName();
        this.family = customer.getLastName();
        this.username = customer.getUsername();
        this.email = customer.getEmail();
        this.credit = customer.getCredit();
        if (customer.getVipExpiration() != null) {
            Duration duration = Duration.between(customer.getVipExpiration(), LocalDateTime.now());
            dayUntilExpireVIP = duration.toDays();
        } else {
            dayUntilExpireVIP = 0;
        }
    }
}
