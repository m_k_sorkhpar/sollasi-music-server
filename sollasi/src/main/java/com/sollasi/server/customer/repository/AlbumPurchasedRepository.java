package com.sollasi.server.customer.repository;

import com.sollasi.security.baseuser.domain.Customer;
import com.sollasi.server.album.domain.Album;
import com.sollasi.server.customer.domain.activities.AlbumPurchased;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AlbumPurchasedRepository extends JpaRepository<AlbumPurchased, Long> {
    AlbumPurchased findByCustomerAndAlbum(Customer customer, Album album);
}
