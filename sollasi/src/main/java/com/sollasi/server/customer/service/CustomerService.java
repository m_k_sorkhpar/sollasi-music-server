package com.sollasi.server.customer.service;

import com.sollasi.IPG.IPGClient;
import com.sollasi.IPG.exception.IPGException;
import com.sollasi.IPG.request.IPGStepOneRequest;
import com.sollasi.IPG.request.IPGStepTwoRequest;
import com.sollasi.IPG.response.IPGStepTwoResponse;
import com.sollasi.base.advicers.SollasiExceptionHandler;
import com.sollasi.security.baseuser.domain.Customer;
import com.sollasi.security.baseuser.repo.CustomerRepository;
import com.sollasi.server.album.domain.Album;
import com.sollasi.server.album.rest.response.AlbumFeedbackResponse;
import com.sollasi.server.album.service.AlbumService;
import com.sollasi.server.customer.domain.FeedbackType;
import com.sollasi.server.customer.domain.TransactionStatus;
import com.sollasi.server.customer.domain.TransactionType;
import com.sollasi.server.customer.domain.activities.*;
import com.sollasi.server.customer.repository.*;
import com.sollasi.server.customer.rest.response.CustomerProfileResponse;
import com.sollasi.server.customer.rest.response.IPGInitResponse;
import com.sollasi.server.customer.rest.response.PurchaseResponse;
import com.sollasi.server.track.domain.Track;
import com.sollasi.server.track.rest.response.TrackFeedbackResponse;
import com.sollasi.server.track.service.TrackService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Service
@Slf4j
public class CustomerService {
    @Value("${com.sollasi.baseURL}")
    private String baseURL;
    @Value("${IPG.provider}")
    private String IPGProvider;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private AlbumPurchasedRepository albumPurchasedRepository;
    @Autowired
    private AlbumFeedbackRepository albumFeedbackRepository;
    @Autowired
    private TrackPurchasedRepository trackPurchasedRepository;
    @Autowired
    private TrackFeedbackRepository trackFeedbackRepository;
    @Autowired
    private AlbumService albumService;
    @Autowired
    private TrackService trackService;
    @Autowired
    private IPGClient IPGClient;
    @Autowired
    private FinancialRepository financialRepository;


    public CustomerProfileResponse getCustomerProfile(String username) throws Exception {
        Customer customerEntity = this.getCustomerEntityByUsername(username);
        return new CustomerProfileResponse(customerEntity);
    }

    public Customer getCustomerEntity(Long id) throws Exception {
        Customer customerEntity = customerRepository.findOne(id);
        if (customerEntity == null) {
            throw new Exception(SollasiExceptionHandler.CUSTOMER_NOT_FOUND);
        }
        return customerEntity;
    }

    public Customer getCustomerEntityByUsername(String username) throws Exception {
        Customer customerEntity = customerRepository.findByUsername(username);
        if (customerEntity == null) {
            throw new Exception(SollasiExceptionHandler.CUSTOMER_NOT_FOUND);
        }
        return customerEntity;
    }

    public IPGInitResponse increaseCredit(String username, Long amount) throws Exception {
        Customer customer = getCustomerEntityByUsername(username);
        //TODO call IPG webservice
        return new IPGInitResponse("http://...");
    }

    public IPGInitResponse chargeVIP(String username) throws Exception {
        Customer customer = getCustomerEntityByUsername(username);
        //TODO call IPG webservice
        return new IPGInitResponse("http://...");
    }

    @Transactional
    public PurchaseResponse purchaseAlbum(String username, Long albumId) throws Exception {
        Customer customer = getCustomerEntityByUsername(username);
        Album album = albumService.getAlbumEntity(albumId);
        if (customer.getCredit() < album.getPrice()) {
            throw new Exception(SollasiExceptionHandler.ALBUM_NOT_ENOUGH_CREDIT);
        } else {
            AlbumPurchased albumPurchased = new AlbumPurchased();
            albumPurchased.setAlbum(album);
            albumPurchased.setPrice(album.getPrice());
            albumPurchased.setCustomer(customer);
            albumPurchased.setPreviousCredit(customer.getCredit());
            albumPurchased.setNewCredit(customer.getCredit() - album.getPrice());
            albumPurchasedRepository.save(albumPurchased);
            customer.setCredit(customer.getCredit() - album.getPrice());
            customerRepository.save(customer);
        }
        return new PurchaseResponse(customer.getCredit());
    }

    @Transactional
    public PurchaseResponse purchaseTrack(String username, Long trackId) throws Exception {
        Customer customer = getCustomerEntityByUsername(username);
        Track track = trackService.getTrackEntity(trackId);
        if (customer.getCredit() < track.getPrice()) {
            throw new Exception(SollasiExceptionHandler.TRACK_NOT_ENOUGH_CREDIT);
        } else {
            TrackPurchased trackPurchased = new TrackPurchased();
            trackPurchased.setTrack(track);
            trackPurchased.setPrice(track.getPrice());
            trackPurchased.setCustomer(customer);
            trackPurchased.setPreviousCredit(customer.getCredit());
            trackPurchased.setNewCredit(customer.getCredit() - track.getPrice());
            trackPurchasedRepository.save(trackPurchased);
            customer.setCredit(customer.getCredit() - track.getPrice());
            customerRepository.save(customer);
        }
        return new PurchaseResponse(customer.getCredit());
    }

    public CustomerProfileResponse updateCustomerProfile(String username, String name, String family, String email) throws Exception {
        Customer customer = getCustomerEntityByUsername(username);
        customer.setFirstName(name);
        customer.setLastName(family);
        customer.setEmail(email);
        customerRepository.save(customer);
        return new CustomerProfileResponse(customer);
    }


    public boolean isCustomerPurchasedTrack(String username, Long trackId) throws Exception {
        Customer customer = this.getCustomerEntityByUsername(username);
        Track track = trackService.getTrackEntity(trackId);
        return isCustomerPurchasedTrack(customer, track);
    }

    public boolean isCustomerPurchasedTrack(Customer customer, Track track) throws Exception {
        if (track.getPrice() == null) {
            return false;
        }
        if (track.getPrice() == 0) {
            return true;
        }
        TrackPurchased trackPurchased = trackPurchasedRepository.findByCustomerAndTrack(customer, track);
        if (trackPurchased != null) {
            return true;
        }
        return isCustomerPurchasedAlbum(customer, track.getAlbum());
    }

    public boolean isCustomerPurchasedAlbum(String username, Long albumId) throws Exception {
        return isCustomerPurchasedAlbum(
                this.getCustomerEntityByUsername(username), albumService.getAlbumEntity(albumId)
        );
    }

    public boolean isCustomerPurchasedAlbum(Customer customer, Album album) {
        AlbumPurchased albumPurchased = albumPurchasedRepository.findByCustomerAndAlbum(customer, album);
        return albumPurchased != null;
    }

    public AlbumFeedbackResponse likeAlbum(String username, Long albumId) throws Exception {
        Album album = albumService.getAlbumEntity(albumId);
        Customer customer = getCustomerEntityByUsername(username);
        AlbumFeedback albumFeedback = albumFeedbackRepository.findByCustomerAndAlbum(customer, album);
        if (albumFeedback == null) {
            albumFeedback = new AlbumFeedback();
            albumFeedback.setAlbum(album);
            albumFeedback.setCustomer(customer);
            albumFeedback.setType(FeedbackType.LIKE);
            albumFeedbackRepository.save(albumFeedback);
            albumService.increaseLikeCounter(album);
        } else {
            if (albumFeedback.getType() != FeedbackType.LIKE) {
                albumFeedback.setType(FeedbackType.LIKE);
                albumFeedbackRepository.save(albumFeedback);
                albumService.increaseLikeCounter(album);
                albumService.decreaseDislikeCounter(album);
            }
        }
        return new AlbumFeedbackResponse(album.getLikeCounter(), album.getDislikeCounter());
    }

    public AlbumFeedbackResponse dislikeAlbum(String username, Long albumId) throws Exception {
        Album album = albumService.getAlbumEntity(albumId);
        Customer customer = getCustomerEntityByUsername(username);
        AlbumFeedback albumFeedback = albumFeedbackRepository.findByCustomerAndAlbum(customer, album);
        if (albumFeedback == null) {
            albumFeedback = new AlbumFeedback();
            albumFeedback.setAlbum(album);
            albumFeedback.setCustomer(customer);
            albumFeedback.setType(FeedbackType.DISLIKE);
            albumFeedbackRepository.save(albumFeedback);
            albumService.increaseDislikeCounter(album);
        } else {
            if (albumFeedback.getType() != FeedbackType.DISLIKE) {
                albumFeedback.setType(FeedbackType.DISLIKE);
                albumFeedbackRepository.save(albumFeedback);
                albumService.increaseDislikeCounter(album);
                albumService.decreaseLikeCounter(album);
            }
        }
        return new AlbumFeedbackResponse(album.getLikeCounter(), album.getDislikeCounter());
    }


    public TrackFeedbackResponse likeTrack(String username, Long trackId) throws Exception {
        Track track = trackService.getTrackEntity(trackId);
        Customer customer = getCustomerEntityByUsername(username);
        TrackFeedback trackFeedback = trackFeedbackRepository.findByCustomerAndTrack(customer, track);
        if (trackFeedback == null) {
            trackFeedback = new TrackFeedback();
            trackFeedback.setTrack(track);
            trackFeedback.setCustomer(customer);
            trackFeedback.setType(FeedbackType.LIKE);
            trackFeedbackRepository.save(trackFeedback);
            trackService.increaseLikeCounter(track);
        } else {
            if (trackFeedback.getType() != FeedbackType.LIKE) {
                trackFeedback.setType(FeedbackType.LIKE);
                trackFeedbackRepository.save(trackFeedback);
                trackService.increaseLikeCounter(track);
                trackService.decreaseDislikeCounter(track);
            }
        }
        return new TrackFeedbackResponse(track.getLikeCounter(), track.getDislikeCounter());
    }

    public TrackFeedbackResponse dislikeTrack(String username, Long trackId) throws Exception {
        Track track = trackService.getTrackEntity(trackId);
        Customer customer = getCustomerEntityByUsername(username);
        TrackFeedback trackFeedback = trackFeedbackRepository.findByCustomerAndTrack(customer, track);
        if (trackFeedback == null) {
            trackFeedback = new TrackFeedback();
            trackFeedback.setTrack(track);
            trackFeedback.setCustomer(customer);
            trackFeedback.setType(FeedbackType.DISLIKE);
            trackFeedbackRepository.save(trackFeedback);
            trackService.increaseDislikeCounter(track);
        } else {
            if (trackFeedback.getType() != FeedbackType.DISLIKE) {
                trackFeedback.setType(FeedbackType.DISLIKE);
                trackFeedbackRepository.save(trackFeedback);
                trackService.increaseDislikeCounter(track);
                trackService.decreaseLikeCounter(track);
            }
        }
        return new TrackFeedbackResponse(track.getLikeCounter(), track.getDislikeCounter());
    }

    public AlbumFeedback getCustomerAlbumFeedback(Customer customer, Album album) {
        return albumFeedbackRepository.findByCustomerAndAlbum(customer, album);
    }

    public TrackFeedback getCustomerTrackFeedback(Customer customer, Track track) {
        return trackFeedbackRepository.findByCustomerAndTrack(customer, track);
    }

    public String IPGInitiateVIP(String username, String customerIp) throws Exception {

        Customer customer = this.getCustomerEntityByUsername(username);

        //TODO init base info table;
        //TODO tax additional value
        Long amount = 15000L;

        FinancialTransaction financialTransaction = new FinancialTransaction();
        financialTransaction.setAmount(amount);
        financialTransaction.setCustomer(customer);
        financialTransaction.setIp(customerIp);
        financialTransaction.setType(TransactionType.BUY_VIP_ACCOUNT);
        financialTransaction.setPreviousVIP(customer.getVipExpiration());
        financialTransaction = financialRepository.save(financialTransaction);

        IPGStepOneRequest request = new IPGStepOneRequest();
        request.setAmount(amount);
        request.setPhoneNumber(customer.getUsername());
        request.setPaymentId(financialTransaction.getId());
        request.setReturnPath(baseURL + IPGProvider);
        try {
            return IPGClient.initiate(request).getPaymentPath();
        } catch (Exception e) {
            financialTransaction.setTransactionStatus(TransactionStatus.FAILED);
            financialRepository.save(financialTransaction);
            throw e;
        }
    }

    public String IPGInitiateIncreaseCredit(String username, Long amount, String customerIp) throws Exception {
        if (amount <= 0) {
            throw new Exception(SollasiExceptionHandler.INVALID_AMOUNT);
        }
        //TODO tax additional value

        Customer customer = this.getCustomerEntityByUsername(username);

        FinancialTransaction financialTransaction = new FinancialTransaction();
        financialTransaction.setAmount(amount);
        financialTransaction.setCustomer(customer);
        financialTransaction.setIp(customerIp);
        financialTransaction.setType(TransactionType.INCREASE_CREDIT);
        financialTransaction.setPreviousCredit(customer.getCredit());
        financialTransaction = financialRepository.save(financialTransaction);

        IPGStepOneRequest request = new IPGStepOneRequest();
        request.setAmount(amount);
        request.setPhoneNumber(customer.getUsername());
        request.setPaymentId(financialTransaction.getId());
        request.setReturnPath(baseURL + IPGProvider);
        return IPGClient.initiate(request).getPaymentPath();
    }

    private void successfulPayment(FinancialTransaction financialTransaction) {
        Customer customer = financialTransaction.getCustomer();
        if (financialTransaction.getType() == TransactionType.BUY_VIP_ACCOUNT) {
            LocalDateTime previousVIPDateTime = financialTransaction.getCustomer().getVipExpiration();
            if (previousVIPDateTime == null) {
                previousVIPDateTime = LocalDateTime.now();
            }
            LocalDateTime newVIPDateTime = previousVIPDateTime.plus(30, ChronoUnit.DAYS);
            customer.setVipExpiration(newVIPDateTime);
            financialTransaction.setNewVIP(newVIPDateTime);
        } else if (financialTransaction.getType() == TransactionType.INCREASE_CREDIT) {
            Long previousCredit = financialTransaction.getPreviousCredit();
            Long newCredit = previousCredit + financialTransaction.getAmount();
            customer.setCredit(newCredit);
            financialTransaction.setNewCredit(newCredit);
        }
        financialTransaction.setTransactionStatus(TransactionStatus.SUCCESSFUL);
        customerRepository.save(customer);
        financialRepository.save(financialTransaction);
    }

    private void rollbackPayment(FinancialTransaction financialTransaction) {
        Customer customer = financialTransaction.getCustomer();
        if (financialTransaction.getType() == TransactionType.BUY_VIP_ACCOUNT) {
            LocalDateTime previousVIPDateTime = financialTransaction.getCustomer().getVipExpiration();
            customer.setVipExpiration(previousVIPDateTime);
            financialTransaction.setNewVIP(previousVIPDateTime);
        } else if (financialTransaction.getType() == TransactionType.INCREASE_CREDIT) {
            Long previousCredit = financialTransaction.getPreviousCredit();
            customer.setCredit(previousCredit);
            financialTransaction.setNewCredit(previousCredit);
        }
        financialTransaction.setTransactionStatus(TransactionStatus.ROLLBACK);
        customerRepository.save(customer);
        financialRepository.save(financialTransaction);
    }

    public FinancialTransaction getFinancialTransactionEntity(Long financialTransactionId) throws Exception {
        FinancialTransaction financialTransaction = financialRepository.findOne(financialTransactionId);
        if (financialTransaction == null) {
            throw new Exception(SollasiExceptionHandler.TRANSACTION_NOT_FOUND);
        }
        return financialTransaction;
    }

    @Transactional
    public Long arianpalCallback(Long financialTransactionId, int status, String refNumber) throws Exception {
        FinancialTransaction financialTransaction = this.getFinancialTransactionEntity(financialTransactionId);
        if (financialTransaction.getTransactionStatus() != TransactionStatus.INITIATE) {
            logger.error("Duplicate callback from arianpal financialTransactionId:[{}]", financialTransaction);
            throw new Exception(SollasiExceptionHandler.DUPLICATE_CALLBACK);
        }
        financialTransaction.setRefNo(refNumber);
        financialTransaction.setStatusResult(String.valueOf(status));

        if (status == 100) {
            this.successfulPayment(financialTransaction);
            try {
                IPGStepTwoRequest ipgStepTwoRequest = new IPGStepTwoRequest();
                ipgStepTwoRequest.setPrice(financialTransaction.getAmount());
                ipgStepTwoRequest.setIPGReference(financialTransaction.getRefNo());
                IPGStepTwoResponse response = IPGClient.verify(ipgStepTwoRequest);
                financialTransaction.setVerifyResult("SUCCESSFUL");
                financialRepository.save(financialTransaction);
                return financialTransaction.getId();
            } catch (IPGException ipgException) {
                logger.error("arianpal verification step throw exception", ipgException);
                financialTransaction.setVerifyResult(ipgException.getMessage());
                rollbackPayment(financialTransaction);
                return null;
            }

        } else {
            financialTransaction.setTransactionStatus(TransactionStatus.FAILED);
        }
        financialRepository.save(financialTransaction);
        return null;
    }

    @Transactional
    public Long payIrCallback(Long financialTransactionId, int status, String refNumber, String message) throws Exception {
        FinancialTransaction financialTransaction = this.getFinancialTransactionEntity(financialTransactionId);
        if (financialTransaction.getTransactionStatus() != TransactionStatus.INITIATE) {
            logger.error("Duplicate callback from payIr financialTransactionId:[{}]", financialTransaction);
            throw new Exception(SollasiExceptionHandler.DUPLICATE_CALLBACK);
        }
        financialTransaction.setRefNo(refNumber);
        financialTransaction.setStatusResult(message);

        if (status == 1) {
            this.successfulPayment(financialTransaction);
            try {
                IPGStepTwoRequest ipgStepTwoRequest = new IPGStepTwoRequest();
                ipgStepTwoRequest.setPrice(financialTransaction.getAmount());
                ipgStepTwoRequest.setIPGReference(financialTransaction.getRefNo());
                IPGStepTwoResponse response = IPGClient.verify(ipgStepTwoRequest);
                financialTransaction.setVerifyResult("SUCCESSFUL");
                financialRepository.save(financialTransaction);
                return financialTransaction.getId();
            } catch (IPGException ipgException) {
                logger.error("arianpal verification step throw exception", ipgException);
                financialTransaction.setVerifyResult(ipgException.getMessage());
                rollbackPayment(financialTransaction);
                return null;
            }
        } else {
            financialTransaction.setTransactionStatus(TransactionStatus.FAILED);
        }
        financialRepository.save(financialTransaction);
        return null;
    }
}
