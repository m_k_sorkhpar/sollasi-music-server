package com.sollasi.server.customer.rest;

import com.sollasi.server.customer.rest.request.UpdateProfileRequest;
import com.sollasi.server.customer.rest.response.CustomerProfileResponse;
import com.sollasi.server.customer.rest.response.PurchaseResponse;
import com.sollasi.server.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@RestController
@RequestMapping("/api/customer")
public class CustomerRest {

    @Autowired
    private CustomerService customerService;

    @GetMapping("/profile")
    @PreAuthorize("hasAnyAuthority('customer')")
    public CustomerProfileResponse getCustomerProfile(Principal principal) throws Exception {
        return customerService.getCustomerProfile(principal.getName());
    }

    @PostMapping("/profile")
    @PreAuthorize("hasAnyAuthority('customer')")
    public CustomerProfileResponse updateCustomerProfile(Principal principal, @RequestBody UpdateProfileRequest request
    ) throws Exception {
        return customerService.updateCustomerProfile(principal.getName(), request.getName(), request.getFamily(), request.getEmail());
    }

    @PostMapping(value = "/chargeVIP")
    @PreAuthorize("hasAnyAuthority('customer')")
    public String chargeVIP(Principal principal, HttpServletRequest request) throws Exception {
        return customerService.IPGInitiateVIP(principal.getName(), request.getRemoteAddr());
    }

    @PostMapping(value = "/increaseCredit")
    @PreAuthorize("hasAnyAuthority('customer')")
    public String increaseCredit(Principal principal, @RequestParam("amount") Long amount, HttpServletRequest request) throws Exception {

        return customerService.IPGInitiateIncreaseCredit(principal.getName(), amount, request.getRemoteAddr());
    }


    @PostMapping("/purchaseAlbum/{albumId}")
    @PreAuthorize("hasAnyAuthority('customer')")
    public PurchaseResponse purchaseAlbum(Principal principal, @PathVariable(name = "albumId") long albumId)
            throws Exception {
        return customerService.purchaseAlbum(principal.getName(), albumId);
    }


    @PostMapping("/purchaseTrack/{trackId}")
    @PreAuthorize("hasAnyAuthority('customer')")
    public PurchaseResponse purchaseTrack(Principal principal, @PathVariable(name = "trackId") long trackId)
            throws Exception {
        return customerService.purchaseTrack(principal.getName(), trackId);
    }
}
