package com.sollasi.server.customer.repository;

import com.sollasi.security.baseuser.domain.Customer;
import com.sollasi.server.album.domain.Album;
import com.sollasi.server.customer.domain.activities.AlbumFeedback;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AlbumFeedbackRepository extends JpaRepository<AlbumFeedback, Long> {
    AlbumFeedback findByCustomerAndAlbum(Customer customer, Album album);
}
