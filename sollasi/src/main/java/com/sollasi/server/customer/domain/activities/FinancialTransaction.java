package com.sollasi.server.customer.domain.activities;

import com.sollasi.security.baseuser.domain.Customer;
import com.sollasi.server.customer.domain.TransactionStatus;
import com.sollasi.server.customer.domain.TransactionType;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "financial_transaction")
@Data
@Accessors
public class FinancialTransaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "financial_transaction_id")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false, columnDefinition = "VARCHAR(50) DEFAULT 'INITIATE' ")
    private TransactionStatus transactionStatus = TransactionStatus.INITIATE;

    @Column(name = "amount", nullable = false)
    private Long amount;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false, updatable = false)
    private TransactionType type;

    @Column(name = "submit_date", nullable = false)
    private LocalDateTime submitDatetime = LocalDateTime.now();

    @ManyToOne(optional = false)
    @JoinColumn(name = "customer_fk")
    private Customer customer;

    @Column(name = "IPG_reference_number")
    private String refNo;

    @Column(name = "IPG_status_result")
    private String statusResult;

    @Column(name = "IPG_verify_result")
    private String verifyResult;

    @Column(name = "customer_previous_VIP_date")
    private LocalDateTime previousVIP;

    @Column(name = "customer_new_VIP_date")
    private LocalDateTime newVIP;

    @Column(name = "customer_previous_credit")
    private Long previousCredit;

    @Column(name = "customer_new_credit")
    private Long newCredit;

    @Column(name = "customer_ip")
    private String ip;
}

