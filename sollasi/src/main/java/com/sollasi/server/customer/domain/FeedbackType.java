package com.sollasi.server.customer.domain;

public enum FeedbackType {
    LIKE,
    DISLIKE
}
