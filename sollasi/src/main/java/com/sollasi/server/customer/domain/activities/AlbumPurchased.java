package com.sollasi.server.customer.domain.activities;

import com.sollasi.security.baseuser.domain.Customer;
import com.sollasi.server.album.domain.Album;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "album_purchased")
@Data
@Accessors
public class AlbumPurchased {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "album_purchased_id")
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "album_fk")
    private Album album;

    @Column(name = "submit_date", nullable = false)
    private LocalDateTime submitDatetime = LocalDateTime.now();

    @ManyToOne(optional = false)
    @JoinColumn(name = "customer_fk")
    private Customer customer;

    @Column(name = "customer_previous_credit")
    private Long previousCredit;

    @Column(name = "customer_new_credit")
    private Long newCredit;

    @Column(name = "album_price")
    private Long price;
}

