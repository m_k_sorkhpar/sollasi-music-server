package com.sollasi.server.customer.domain.activities;

import com.sollasi.security.baseuser.domain.Customer;
import com.sollasi.server.customer.domain.FeedbackType;
import com.sollasi.server.track.domain.Track;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "track_feedback")
@Data
@Accessors
public class TrackFeedback {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "track_feedback_id")
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "track_fk")
    private Track track;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private FeedbackType type;

    @Column(name = "submit_date", nullable = false)
    private LocalDateTime submitDatetime = LocalDateTime.now();

    @ManyToOne(optional = false)
    @JoinColumn(name = "customer_fk")
    private Customer customer;
}

