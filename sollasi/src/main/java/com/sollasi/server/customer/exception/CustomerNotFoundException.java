package com.sollasi.server.customer.exception;

import com.sollasi.base.exceptions.notfound.BaseEntityNotFoundException;

public class CustomerNotFoundException extends BaseEntityNotFoundException {

    public CustomerNotFoundException(Long id) {
        super(CustomerNotFoundException.class, id);
    }

    public CustomerNotFoundException() {
        super(CustomerNotFoundException.class, null);
    }
}
