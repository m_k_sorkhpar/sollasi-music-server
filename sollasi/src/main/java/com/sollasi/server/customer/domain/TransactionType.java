package com.sollasi.server.customer.domain;

public enum TransactionType {
    BUY_VIP_ACCOUNT,
    INCREASE_CREDIT
}
