package com.sollasi.server.customer.domain.activities;

import com.sollasi.server.album.domain.Album;
import com.sollasi.security.baseuser.domain.Customer;
import com.sollasi.server.customer.domain.FeedbackType;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "album_feedback")
@Data
@Accessors
public class AlbumFeedback {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "album_feedback_id")
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "album_fk")
    private Album album;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private FeedbackType type;

    @Column(name = "submit_date", nullable = false)
    private LocalDateTime submitDatetime = LocalDateTime.now();

    @ManyToOne(optional = false)
    @JoinColumn(name = "customer_fk")
    private Customer customer;

}


