package com.sollasi.server.customer.repository;

import com.sollasi.server.category.domain.Category;
import com.sollasi.server.customer.domain.activities.FinancialTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FinancialRepository extends JpaRepository<FinancialTransaction, Long> {
}
