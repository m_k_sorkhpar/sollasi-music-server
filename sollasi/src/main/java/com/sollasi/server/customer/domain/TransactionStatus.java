package com.sollasi.server.customer.domain;

public enum TransactionStatus {
    INITIATE,
    SUCCESSFUL,
    FAILED,
    ROLLBACK
}
