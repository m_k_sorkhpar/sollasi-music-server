package com.sollasi.server.artist.rest.response;

import com.sollasi.server.artist.domain.Artist;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArtistResponse {
    private long total;

    private List<ArtistResponseElements> artists;

    public void add(Artist artist) {
        if (artists == null) {
            artists = new ArrayList<>();
        }
        artists.add(new ArtistResponseElements(artist));
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public class ArtistResponseElements {

        private Long id;

        private String name;

        private String bio;

        private Long mediaId;

        public ArtistResponseElements(Artist artist) {
            this.id = artist.getId();
            this.name = artist.getName();
            this.bio = artist.getBio();
            this.mediaId = artist.getImage() != null ? artist.getImage().getId() : null;
        }
    }
}