package com.sollasi.server.artist.service;

import com.sollasi.base.advicers.SollasiExceptionHandler;
import com.sollasi.base.util.GridPagination;
import com.sollasi.security.baseuser.domain.Customer;
import com.sollasi.server.album.rest.response.AlbumResponse;
import com.sollasi.server.album.service.AlbumService;
import com.sollasi.server.artist.domain.Artist;
import com.sollasi.server.artist.repository.ArtistRepository;
import com.sollasi.server.artist.rest.response.ArtistDetailResponse;
import com.sollasi.server.artist.rest.response.ArtistResponse;
import com.sollasi.server.track.rest.response.TrackResponse;
import com.sollasi.server.track.service.TrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public class ArtistService {
    @Autowired
    private ArtistRepository artistRepository;
    @Autowired
    private TrackService trackService;
    @Autowired
    private AlbumService albumService;

    public Artist getArtistEntity(long artistId) throws Exception {
        Artist artist = artistRepository.findOne(artistId);
        if (artist == null) {
            throw new Exception(SollasiExceptionHandler.ARTIST_NOT_FOUND);
        }
        return artist;
    }

    public ArtistResponse getArtists(int offset, int limit) {
        ArtistResponse response = new ArtistResponse();

        Page<Artist> artists = artistRepository.findByOrderByNameAsc(new GridPagination(offset, limit));
        for (Artist artist : artists.getContent()) {
            response.add(artist);
        }
        response.setTotal(artists.getTotalElements());
        return response;
    }

    public ArtistDetailResponse getArtistDetail(Long artistId) throws Exception {
        ArtistDetailResponse response = new ArtistDetailResponse();
        Artist artist = getArtistEntity(artistId);
        response.setId(artist.getId());
        response.setName(artist.getName());
        response.setBio(artist.getBio());
        response.setMediaId(artist.getImage() != null ? artist.getImage().getId() : null);
        AlbumResponse albums = albumService.getAlbums(0, 20, artist.getId());
        TrackResponse tracks = trackService.getTracks(0, 20, artist.getId(), null, null);
        response.setArtistAlbums(albums);
        response.setArtistTracks(tracks);
        return response;
    }

    public ArtistResponse searchArtist(Customer customer, String word) {
        ArtistResponse artistResponse = new ArtistResponse();
        Page<Artist> artists = artistRepository.search(word, new GridPagination(0, 10));
        for (Artist artist : artists.getContent()) {
            artistResponse.add(artist);
        }
        artistResponse.setTotal(artists.getContent().size());
        return artistResponse;
    }
}
