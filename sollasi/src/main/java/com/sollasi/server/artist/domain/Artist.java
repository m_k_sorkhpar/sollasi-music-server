package com.sollasi.server.artist.domain;

import com.sollasi.server.media.domain.Media;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Entity
@Table(name = "artist")
@Data
@Accessors
public class Artist {

    @Id
    @Column(name = "artist_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "bio", columnDefinition = "TEXT")
    private String bio;

    @Column(name = "income_factor", columnDefinition = "FLOAT DEFAULT 1")
    private Float incomeFactor;

    @ManyToOne
    @JoinColumn(name = "image_fk")
    private Media image;


}