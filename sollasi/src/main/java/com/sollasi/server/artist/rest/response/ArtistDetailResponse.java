package com.sollasi.server.artist.rest.response;

import com.sollasi.server.album.rest.response.AlbumResponse;
import com.sollasi.server.track.rest.response.TrackResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArtistDetailResponse {
    private Long id;

    private String name;

    private String bio;

    private Long mediaId;

    private AlbumResponse artistAlbums;

    private TrackResponse artistTracks;
}