package com.sollasi.server.artist.repository;

import com.sollasi.server.artist.domain.Artist;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ArtistRepository extends JpaRepository<Artist, Long> {

    Page<Artist> findByOrderByNameAsc(Pageable pageable);


    @Query(value = "select a from Artist a where " +
            "a.name like concat('%',:word,'%') or a.bio like concat('%',:word,'%')")
//TODO order by
    Page<Artist> search(@Param("word") String word, Pageable gridPagination);

}
