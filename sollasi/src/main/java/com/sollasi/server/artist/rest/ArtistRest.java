package com.sollasi.server.artist.rest;

import com.sollasi.server.artist.rest.response.ArtistDetailResponse;
import com.sollasi.server.artist.rest.response.ArtistResponse;
import com.sollasi.server.artist.service.ArtistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/artist")
public class ArtistRest {

    @Autowired
    private ArtistService artistService;

    @GetMapping("/")
    public ArtistResponse getArtists(@RequestParam(value = "offset") int offset, @RequestParam(value = "limit") int limit) {
        return artistService.getArtists(offset, limit);

    }

    @GetMapping("/{artistId}")
    public ArtistDetailResponse getArtists(@PathVariable("artistId") Long artistId) throws Exception {
        return artistService.getArtistDetail(artistId);

    }
}
