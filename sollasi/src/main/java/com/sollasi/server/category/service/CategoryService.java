package com.sollasi.server.category.service;

import com.sollasi.base.advicers.SollasiExceptionHandler;
import com.sollasi.base.util.GridPagination;
import com.sollasi.server.category.domain.Category;
import com.sollasi.server.category.repository.CategoryRepository;
import com.sollasi.server.category.rest.response.CategoryResponse;
import com.sollasi.server.track.rest.response.TrackResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;

    public Category getCategoryEntity(Long categoryId) throws Exception {
        Category category = categoryRepository.findOne(categoryId);
        if (category == null) {
            throw new Exception(SollasiExceptionHandler.CATEGORY_NOT_FOUND);
        }
        return category;
    }

    public CategoryResponse getAllCategories(int offset, int limit) {
        CategoryResponse categoryResponse = new CategoryResponse();
        Page<Category> categories = categoryRepository.findAllByOrderByName(new GridPagination(offset, limit));
        for (Category category : categories.getContent()) {
            categoryResponse.add(category);
        }
        categoryResponse.setTotal(categories.getTotalElements());
        return categoryResponse;
    }

    public List<CategoryResponse> searchCategoryName(String categoryName) {

        return null;
    }

    public TrackResponse getCategoryTracks(Long categortId) {
        return null;
    }
}
