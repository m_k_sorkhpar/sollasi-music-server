package com.sollasi.server.category.domain;

import com.sollasi.server.media.domain.Media;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Entity
@DiscriminatorValue("music_category")
@Data
@Accessors
public class Category {

    @Id
    @Column(name = "category_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "image_fk")
    private Media image;

}

