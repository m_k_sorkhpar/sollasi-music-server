package com.sollasi.server.category.rest.response;

import com.sollasi.server.category.domain.Category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryResponse {
    private long total;
    private List<CategoryResponseElement> categories;

    public void add(Category category) {
        if (categories == null) {
            categories = new ArrayList<>();
        }
        categories.add(new CategoryResponse.CategoryResponseElement(category));
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public class CategoryResponseElement {
        private Long id;
        private String name;
        private Long mediaId;

        public CategoryResponseElement(Category category) {
            this.id = category.getId();
            this.name = category.getName();
            this.mediaId = category.getImage() == null ? category.getImage().getId() : null;
        }
    }
}
