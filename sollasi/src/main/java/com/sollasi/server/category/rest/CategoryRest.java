package com.sollasi.server.category.rest;

import com.sollasi.server.category.rest.response.CategoryResponse;
import com.sollasi.server.category.service.CategoryService;
import com.sollasi.server.track.rest.response.TrackResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/category")
public class CategoryRest {
    @Autowired
    private CategoryService categoryService;

    @GetMapping("/")
    public CategoryResponse getCategories(@RequestParam(value = "offset") int offset, @RequestParam(value = "limit") int limit) {
        return categoryService.getAllCategories(offset, limit);
    }

    @GetMapping("/search")
    public List<CategoryResponse> getCategories(@RequestParam("categoryNam") String categoryName) {
        return categoryService.searchCategoryName(categoryName);
    }

    @GetMapping("/{categoryId}")
    public TrackResponse getCategories(@PathVariable("categoryId") Long categoryId) {
        return categoryService.getCategoryTracks(categoryId);
    }

}
