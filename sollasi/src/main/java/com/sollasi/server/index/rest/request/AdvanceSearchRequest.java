package com.sollasi.server.index.rest.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdvanceSearchRequest {
    private String trackName;
    private String albumName;
    private String artistName;
    private Long categoryId;
    private Long fromDateTimestamp;
    private Long toDateTimestamp;
}
