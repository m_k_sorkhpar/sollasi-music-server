package com.sollasi.server.index.rest.response;

import com.sollasi.server.album.rest.response.AlbumResponse;
import com.sollasi.server.track.rest.response.TrackResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IndexResponse {
    private TrackResponse topTracks;
    private AlbumResponse topAlbums;
    private TrackResponse newestTracks;
    private AlbumResponse newestAlbum;
    private TrackResponse promotedTracks;

}
