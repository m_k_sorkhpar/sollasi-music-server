package com.sollasi.server.index.rest.response;

import com.sollasi.server.album.rest.response.AlbumResponse;
import com.sollasi.server.artist.rest.response.ArtistResponse;
import com.sollasi.server.track.rest.response.TrackResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchResponse {
    private TrackResponse tracks;
    private AlbumResponse albums;
    private ArtistResponse artists;
}
