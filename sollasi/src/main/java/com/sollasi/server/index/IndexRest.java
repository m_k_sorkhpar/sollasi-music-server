package com.sollasi.server.index;

import com.sollasi.server.index.rest.request.AdvanceSearchRequest;
import com.sollasi.server.index.rest.response.FavoritesResponse;
import com.sollasi.server.index.rest.response.IndexResponse;
import com.sollasi.server.index.rest.response.SearchResponse;
import com.sollasi.server.index.service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/api/")
public class IndexRest {
    @Autowired
    private IndexService indexService;

    @GetMapping("/index")
    public IndexResponse getIndexData() {
        return indexService.getIndexData();
    }

    @GetMapping("/customerFavorites")
    public FavoritesResponse getCustomerFavorites(Principal principal) throws Exception {
        return indexService.getFavorites(principal.getName());
    }

    @GetMapping("/search")
    public SearchResponse search(Principal principal, @RequestParam("word") String word) throws Exception {
        return indexService.search(principal.getName(), word);
    }

    @PostMapping("/advanceSearch")
    public SearchResponse advanceSearch(@RequestBody AdvanceSearchRequest request) {
        return indexService.advanceSearch(request.getTrackName(), request.getAlbumName(), request.getArtistName(),
                request.getCategoryId(), request.getFromDateTimestamp(), request.getToDateTimestamp());
    }

}
