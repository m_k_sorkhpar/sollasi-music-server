package com.sollasi.server.index.service;

import com.sollasi.security.baseuser.domain.Customer;
import com.sollasi.server.album.rest.response.AlbumResponse;
import com.sollasi.server.album.service.AlbumService;
import com.sollasi.server.artist.rest.response.ArtistResponse;
import com.sollasi.server.artist.service.ArtistService;
import com.sollasi.server.customer.service.CustomerService;
import com.sollasi.server.index.rest.response.FavoritesResponse;
import com.sollasi.server.index.rest.response.IndexResponse;
import com.sollasi.server.index.rest.response.SearchResponse;
import com.sollasi.server.news.service.NewsService;
import com.sollasi.server.track.rest.response.TrackResponse;
import com.sollasi.server.track.service.TrackService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
@Slf4j
public class IndexService {
    @Autowired
    private CustomerService customerService;
    @Autowired
    private TrackService trackService;
    @Autowired
    private AlbumService albumService;
    @Autowired
    private ArtistService artistService;
    @Autowired
    private NewsService newsService;

    private IndexResponse indexResponse;

    @PostConstruct
    private void init() {
        this.indexResponse = new IndexResponse();
    }

    @Scheduled(fixedDelay = 30 * 60 * 1000)
    public void fetchIndexResponse() throws Exception {
        try {

            AlbumResponse newestAlbumResponse = albumService.getAlbums(0, 10, null);
            newestAlbumResponse.setTotal(newestAlbumResponse.getAlbums().size());

            TrackResponse newestTracks = trackService.getTracks(0, 10, null, null, null);
            newestTracks.setTotal(newestTracks.getTracks().size());

            TrackResponse promotedTracks = trackService.getPromotedTrack();

            this.indexResponse.setNewestAlbum(newestAlbumResponse);
            this.indexResponse.setNewestTracks(newestTracks);
            this.indexResponse.setPromotedTracks(promotedTracks);
            //TODO fetch top albums and tracks
            this.indexResponse.setTopAlbums(newestAlbumResponse);
            this.indexResponse.setTopTracks(newestTracks);
        } catch (Exception e) {
            logger.error("fetch index data scheduler throw exception", e);
        }
    }

    public IndexResponse getIndexData() {
        return this.indexResponse;
    }

    public FavoritesResponse getFavorites(String username) throws Exception {
        Customer customer = customerService.getCustomerEntityByUsername(username);
        //TODO get customer favorites
        return null;
    }

    public SearchResponse search(String username, String word) throws Exception {
        Customer customer = customerService.getCustomerEntityByUsername(username);
        TrackResponse trackResponse = trackService.searchTrack(customer, word);
        AlbumResponse albumResponse = albumService.searchAlbum(customer, word);
        ArtistResponse artistResponse = artistService.searchArtist(customer, word);
        return new SearchResponse(trackResponse, albumResponse, artistResponse);
    }

    public SearchResponse advanceSearch(String trackName, String albumName, String artistName, Long categoryId,
                                        Long fromDateTimestamp, Long toDateTimestamp) {
        //todo search advance
        return null;
    }
}
