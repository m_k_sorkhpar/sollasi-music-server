package com.sollasi.server.media.controller;


import com.sollasi.server.media.domain.Media;
import com.sollasi.server.media.service.MediaService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Controller
@RequestMapping("/api/media")
public class MediaCtrl {


    private MediaService mediaService;

    @Value("${com.sollasi.upload.cover}")
    private String coverPath;
    @Value("${com.sollasi.upload.coverSmall}")
    private String coverSmallPath;

    @Autowired
    public MediaCtrl(MediaService mediaService) {
        this.mediaService = mediaService;
    }


    @GetMapping(value = "/{mediaId}")
    public ResponseEntity<byte[]> getMedia(@PathVariable("mediaId") final Long mediaId) throws Exception {
        Media media = mediaService.getMediaEntity(mediaId);
        byte[] fileContents = Files.readAllBytes(Paths.get(coverPath + File.separator + media.getPath()));
        HttpHeaders headers = new HttpHeaders();
        headers.add("Expires", new Date(System.currentTimeMillis() + 31536000000L).toString()); // A year from now
        headers.add("fileName", mediaId + media.getExtension());
        headers.add("Cache-Control", CacheControl.maxAge(10, TimeUnit.DAYS).cachePublic().getHeaderValue());
        return ResponseEntity.ok()
                .headers(headers)
                .contentLength(fileContents.length)
                .contentType(StringUtils.isEmpty(media.getMimeType()) ? MediaType.APPLICATION_OCTET_STREAM :
                        MediaType.parseMediaType(media.getMimeType())
                )
                .body(fileContents);
        //IOUtils.copy(mediaService.getCoverInputStream(mediaId), response.getOutputStream());
    }

    @GetMapping(value = "/small/{mediaId}")
    public ResponseEntity<byte[]> getSmallMedia(@PathVariable("mediaId") final Long mediaId) throws Exception {
        Media media = mediaService.getMediaEntity(mediaId);
        byte[] fileContents = Files.readAllBytes(Paths.get(coverSmallPath + File.separator + media.getSmallPath()));
        HttpHeaders headers = new HttpHeaders();
        headers.add("Expires", new Date(System.currentTimeMillis() + 31536000000L).toString()); // A year from now
        headers.add("fileName", mediaId + media.getExtension());
        headers.add("Cache-Control", CacheControl.maxAge(10, TimeUnit.DAYS).cachePublic().getHeaderValue());
        return ResponseEntity.ok()
                .headers(headers)
                .contentLength(fileContents.length)
                .contentType(StringUtils.isEmpty(media.getMimeType()) ? MediaType.APPLICATION_OCTET_STREAM :
                        MediaType.parseMediaType(media.getMimeType())
                )
                .body(fileContents);
    }
}
