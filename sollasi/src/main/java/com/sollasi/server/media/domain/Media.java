package com.sollasi.server.media.domain;

import lombok.Data;

import javax.persistence.*;


@Entity
@Table(name = "media_files")
@Data
public class Media {

    @Id
    @Column(name = "media_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Basic
    @Column(name = "media_path", nullable = false)
    private String path;

    @Basic
    @Column(name = "small_media_path")
    private String smallPath;

    @Basic
    @Column(name = "mime_type")
    private String mimeType;

    @Basic
    @Column(name = "extension")
    private String extension;

    @Basic
    @Column(name = "has_public_access", nullable = false, columnDefinition = "BOOLEAN DEFAULT TRUE")
    private Boolean publicAccess;


}