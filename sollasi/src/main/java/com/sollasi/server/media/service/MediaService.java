package com.sollasi.server.media.service;

import com.sollasi.base.advicers.SollasiExceptionHandler;
import com.sollasi.server.media.domain.Media;
import com.sollasi.server.media.repository.MediaRepository;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MimeType;
import org.apache.tika.mime.MimeTypeException;
import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.HashMap;

@Service
@Slf4j
public class MediaService {

    private MediaRepository mediaRepository;

    @Value("${com.sollasi.upload.cover}")
    private String coverPath;
    @Value("${com.sollasi.upload.coverSmall}")
    private String coverSmallPath;

    HashMap<Long, String> mediaFiles = new HashMap<>();
    HashMap<Long, MediaMimeAndExtension> mimeFiles = new HashMap<>();


    @Autowired
    public MediaService(MediaRepository mediaRepository) {
        this.mediaRepository = mediaRepository;
    }

    @Transactional
    public Media uploadFile(MultipartFile upload, String path) throws Exception {

        String[] fileType = upload.getOriginalFilename().split("\\.");
        File file = null;
        try {

            Media uploader = new Media();
            mediaRepository.save(uploader);
            file = new File(path + File.separator + uploader.getId() + "." + fileType[fileType.length - 1]);
            FileCopyUtils.copy(upload.getBytes(), file);
            uploader.setPath("avatars/" + uploader.getId() + "." + fileType[fileType.length - 1]);
            mediaRepository.save(uploader);
            return uploader;
        } catch (Exception e) {
            e.printStackTrace();
            if (file != null && file.isFile())
                FileSystemUtils.deleteRecursively(file);
            throw new Exception("error while uploading");
        }
    }

    public Resource downloadFile(String path) {
        //add some authorization
        Resource resource = new FileSystemResource(path);
        return resource;
    }

    public Media getMediaEntity(Long mediaId) throws Exception {
        Media media = mediaRepository.findOne(mediaId);
        if (media == null) {
            throw new Exception(SollasiExceptionHandler.MEDIA_NOT_FOUND);
        }
        if (media.getPath() != null) {
            boolean changed = false;
            if (media.getMimeType() == null) {
                MediaMimeAndExtension mime = getActualFileExtension(new FileInputStream(
                        coverPath + File.separator + media.getPath()
                ));
                media.setMimeType(mime.getMimeType());
                media.setExtension(mime.getExtension());
                changed = true;
            }
            if (media.getSmallPath() == null) {
                scaleByWidth(
                        new File(coverPath + File.separator + media.getPath()),
                        new File(coverSmallPath + File.separator + media.getPath()),
                        media.getExtension(),
                        256
                );
                media.setSmallPath(media.getPath());
                changed = true;
            }
            if (changed) {
                mediaRepository.save(media);
            }
        }
        mediaFiles.put(mediaId, media.getPath());
        mimeFiles.put(mediaId, new MediaMimeAndExtension(media.getMimeType(), media.getExtension()));
        return media;
    }

    public static void scaleByWidth(File source, File target, String extension, int xSize) throws IOException {
        extension = extension.replace(".", "");
        BufferedImage im = ImageIO.read(source);
        double aspectRatio = ((double) im.getHeight())
                / im.getWidth();
        int ySize = (int) (xSize * aspectRatio);
        if (ySize == 0) {
            ySize = 1;
        }
        BufferedImage scaled;
        if (im.getWidth() > xSize || im.getHeight() > ySize) {
            scaled = Scalr.resize(im, Scalr.Method.SPEED,
                    Scalr.Mode.FIT_TO_WIDTH,
                    xSize,
                    ySize,
                    Scalr.OP_ANTIALIAS);
        } else {
            scaled = im;
        }
        try (FileOutputStream baos = new FileOutputStream(target)) {
            ImageIO.write(scaled, extension, baos);
        } catch (IOException exp) {
            logger.error("converting image exception", exp);
        }
    }

    /*
    https://www.htmlgoodies.com/beyond/java/create-high-quality-thumbnails-using-the-imgscalr-library.html
    BufferedImage thumbnail = Scalr.resize(image,
                                       Scalr.Method.SPEED,
                                       Scalr.Mode.FIT_TO_WIDTH,
                                       150,
                                       100,
                                       Scalr.OP_ANTIALIAS);
     */
    /*public byte[] getCoverBytes(Long mediaId) throws Exception {
        if (!mediaFiles.containsKey(mediaId)) {
            getMediaEntity(mediaId);
        }
        return Files.readAllBytes(Paths.get(
                coverPath + File.separator + mediaFiles.get(mediaId)
        ));
    }

    public byte[] getSmallCoverBytes(Long mediaId) throws Exception {
        if (!mediaFiles.containsKey(mediaId)) {
            getMediaEntity(mediaId);
        }
        return Files.readAllBytes(Paths.get(
                coverSmallPath + File.separator + mediaFiles.get(mediaId)
        ));
    }*/

    private MediaMimeAndExtension getActualFileExtension(InputStream fileInputStream) throws IOException, MimeTypeException {
        TikaConfig config = TikaConfig.getDefaultConfig();
        InputStream bufferedIn = new BufferedInputStream(fileInputStream);
        org.apache.tika.mime.MediaType mediaType = config.getMimeRepository().detect(bufferedIn, new Metadata());
        String mediaTypeString = mediaType.toString();
        MimeType mimeType = config.getMimeRepository().forName(mediaTypeString);
        return new MediaMimeAndExtension(mediaTypeString, mimeType.getExtension());
    }


    @Getter
    @Setter
    @AllArgsConstructor
    private class MediaMimeAndExtension {
        String mimeType;
        String extension;
    }
}
