package com.sollasi.server.media.controller.transfer;


import com.sollasi.server.media.domain.Media;
import lombok.Data;

@Data
public class MediaResponse {
    Media mediaFile;
}
