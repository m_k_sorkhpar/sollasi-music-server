package com.sollasi.server.playlist.rest.response;

import com.sollasi.server.track.rest.response.TrackResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlaylistDetailResponse {

    private Long id;

    private String name;

    private TrackResponse playlistTracks;

}
