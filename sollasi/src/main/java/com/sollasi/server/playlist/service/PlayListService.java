package com.sollasi.server.playlist.service;

import com.sollasi.base.advicers.SollasiExceptionHandler;
import com.sollasi.security.baseuser.domain.Customer;
import com.sollasi.server.customer.service.CustomerService;
import com.sollasi.server.playlist.domain.Playlist;
import com.sollasi.server.playlist.repository.PlaylistRepository;
import com.sollasi.server.playlist.rest.response.PlaylistDetailResponse;
import com.sollasi.server.playlist.rest.response.PlaylistResponse;
import com.sollasi.server.track.domain.Track;
import com.sollasi.server.track.rest.response.TrackResponse;
import com.sollasi.server.track.service.TrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PlayListService {
    @Autowired
    private CustomerService customerService;
    @Autowired
    private PlaylistRepository playlistRepository;
    @Autowired
    private TrackService trackService;

    public Playlist getPlaylistEntity(Long playlistId) throws Exception {
        Playlist playlist = playlistRepository.findOne(playlistId);
        if (playlist == null) {
            throw new Exception(SollasiExceptionHandler.PLAYLIST_NOT_FOUND);
        }
        return playlist;
    }

    public Playlist getPlaylistEntity(Long playlistId, Customer customer) throws Exception {
        Playlist playlist = getPlaylistEntity(playlistId);
        if (!playlist.getCustomer().getId().equals(customer.getId())) {
            throw new Exception(SollasiExceptionHandler.PLAY_LIST_IS_NOT_BELONGS_TO_CUSTOMER);
        }
        return playlist;
    }

    public List<PlaylistResponse> getCustomerPlaylists(String username) throws Exception {
        List<PlaylistResponse> response = new ArrayList<>();
        Customer customer = customerService.getCustomerEntityByUsername(username);
        for (Playlist playlist : playlistRepository.findByCustomerOrderByIdDesc(customer)) {
            response.add(new PlaylistResponse(playlist.getId(), playlist.getName(),
                    playlist.getTracks() != null ? playlist.getTracks().size() : 0));
        }
        return response;
    }

    public PlaylistDetailResponse getPlaylistDetail(String username, Long playlistId) throws Exception {
        Customer customer = customerService.getCustomerEntityByUsername(username);
        Playlist playlist = getPlaylistEntity(playlistId, customer);
        TrackResponse tracks = new TrackResponse();
        for (Track track : playlist.getTracks()) {
            tracks.add(track);
        }
        tracks.setTotal(playlist.getTracks().size());
        return new PlaylistDetailResponse(playlist.getId(), playlist.getName(), tracks);
    }

    public void addTrackToPlaylist(String username, Long playlistId, Long trackId) throws Exception {
        Customer customer = customerService.getCustomerEntityByUsername(username);
        Playlist playlist = getPlaylistEntity(playlistId, customer);
        Track track = trackService.getTrackEntity(trackId);
        playlist.getTracks().add(track);
        playlistRepository.save(playlist);
    }

    public void removeTrackToPlaylist(String username, Long playlistId, Long trackId) throws Exception {
        Customer customer = customerService.getCustomerEntityByUsername(username);
        Playlist playlist = getPlaylistEntity(playlistId, customer);
        Track track = trackService.getTrackEntity(trackId);
        playlist.getTracks().remove(track);
        playlistRepository.save(playlist);

    }

    public PlaylistResponse createCustomerPlaylist(String username, String name) throws Exception {
        Customer customer = customerService.getCustomerEntityByUsername(username);
        Playlist playlist = new Playlist();
        playlist.setCustomer(customer);
        playlist.setName(name);
        playlistRepository.save(playlist);
        return new PlaylistResponse(playlist.getId(), playlist.getName(), 0);
    }

    public void updateCustomerPlaylist(String username, Long playlistId, String name) throws Exception {
        Customer customer = customerService.getCustomerEntityByUsername(username);
        Playlist playlist = getPlaylistEntity(playlistId, customer);
        playlist.setName(name);
        playlistRepository.save(playlist);
    }

    public void removeCustomerPlaylist(String username, Long playlistId) throws Exception {
        Customer customer = customerService.getCustomerEntityByUsername(username);
        Playlist playlist = getPlaylistEntity(playlistId, customer);
        playlistRepository.delete(playlist);
    }
}
