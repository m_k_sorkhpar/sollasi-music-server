package com.sollasi.server.playlist.repository;

import com.sollasi.security.baseuser.domain.Customer;
import com.sollasi.server.playlist.domain.Playlist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlaylistRepository extends JpaRepository<Playlist, Long> {

    List<Playlist> findByCustomerOrderByIdDesc(Customer customer);

    Playlist findByIdAndCustomer(Long id, Customer customer);

}
