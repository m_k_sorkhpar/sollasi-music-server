package com.sollasi.server.playlist.domain;

import com.sollasi.security.baseuser.domain.Customer;
import com.sollasi.server.track.domain.Track;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "playlist")
@Data
@Accessors
public class Playlist {

    @Id
    @Column(name = "playlist_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_fk")
    private Customer customer;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "playlist_m2m_track", joinColumns = {
            @JoinColumn(name = "playlist_fk", nullable = false)},
            inverseJoinColumns = {@JoinColumn(name = "track_fk",
                    nullable = false)})
    //TODO move it tho third table for pagination and optimize
    private Set<Track> tracks;

}

