package com.sollasi.server.playlist.rest;

import com.sollasi.server.playlist.rest.request.PlaylistModificationRequest;
import com.sollasi.server.playlist.rest.request.PlaylistRequest;
import com.sollasi.server.playlist.rest.response.PlaylistDetailResponse;
import com.sollasi.server.playlist.rest.response.PlaylistResponse;
import com.sollasi.server.playlist.service.PlayListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/playlist")
public class PlaylistRest {

    @Autowired
    private PlayListService playListService;

    @GetMapping("/")
    public List<PlaylistResponse> getCustomerPlayLists(Principal principal) throws Exception {
        return playListService.getCustomerPlaylists(principal.getName());
    }

    @PostMapping("/")
    public PlaylistResponse createCustomerPlayLists(Principal principal, @RequestBody PlaylistRequest request) throws Exception {
        return playListService.createCustomerPlaylist(principal.getName(), request.getName());
    }

    @GetMapping("/{playlistId}")
    public PlaylistDetailResponse getCustomerPlayLists(Principal principal,
                                                       @PathVariable("playlistId") Long playlistId)
            throws Exception {
        return playListService.getPlaylistDetail(principal.getName(), playlistId);
    }

    @PostMapping("/{playlistId}")
    public void updateCustomerPlayLists(Principal principal, @PathVariable("playlistId") Long playlistId,
                                        @RequestBody PlaylistRequest request) throws Exception {
        playListService.updateCustomerPlaylist(principal.getName(), playlistId, request.getName());
    }

    @DeleteMapping("/{playlistId}")
    public void updateCustomerPlayLists(Principal principal, @PathVariable("playlistId") Long playlistId) throws Exception {
        playListService.removeCustomerPlaylist(principal.getName(), playlistId);
    }

    @GetMapping("/add/{playlistId}")
    public void addTrackToPlaylist(Principal principal,
                                   @PathVariable("playlistId") Long playlistId,
                                   PlaylistModificationRequest request)
            throws Exception {
        playListService.addTrackToPlaylist(principal.getName(), playlistId, request.getTrackId());
    }

    @GetMapping("/remove/{playlistId}")
    public void removeTrackToPlaylist(Principal principal,
                                      @PathVariable("playlistId") Long playlistId,
                                      PlaylistModificationRequest request)
            throws Exception {
        playListService.removeTrackToPlaylist(principal.getName(), playlistId, request.getTrackId());
    }

}
