package com.sollasi.server.playlist.rest.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlaylistResponse {

    private Long id;

    private String name;

    private int numberOfTracks;
}
