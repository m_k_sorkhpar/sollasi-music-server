package com.sollasi.server.album.rest.response;

import com.sollasi.base.util.DateUtils;
import com.sollasi.server.album.domain.Album;
import com.sollasi.server.category.domain.Category;
import com.sollasi.server.track.rest.response.TrackResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AlbumDetailResponse {

    private Long id;

    private String name;

    private long likeCounter;

    private long dislikeCounter;

    private String publishDate;

    private Long price;

    private String artist;

    private Long mediaId;

    private TrackResponse tracks;

    private List<String> categories;

    private boolean liked;

    private boolean disliked;

    private boolean purchased;

    public AlbumDetailResponse(Album album, TrackResponse tracks, boolean isPurchased, boolean isLiked, boolean isDisliked) {
        this.id = album.getId();
        this.name = album.getName();
        this.likeCounter = album.getLikeCounter();
        this.dislikeCounter = album.getDislikeCounter();
        this.publishDate = album.getPublishDate() != null ? DateUtils.toPersianDateTime(album.getPublishDate()) : null;
        this.price = album.getPrice();
        this.artist = album.getArtist() != null ? album.getArtist().getName() : null;
        this.mediaId = album.getImage() != null ? album.getImage().getId() : null;
        this.tracks = tracks;
        if (album.getCategories() != null && album.getCategories().size() != 0) {
            this.categories = new ArrayList<String>();
            for (Category category : album.getCategories()) {
                this.categories.add(category.getName());
            }
        }
        this.purchased = isPurchased;
        this.liked = isLiked;
        this.disliked = isDisliked;
    }
}
