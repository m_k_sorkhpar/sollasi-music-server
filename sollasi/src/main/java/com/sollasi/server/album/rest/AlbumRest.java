package com.sollasi.server.album.rest;


import com.sollasi.server.album.rest.request.AlbumRequest;
import com.sollasi.server.album.rest.response.AlbumDetailResponse;
import com.sollasi.server.album.rest.response.AlbumFeedbackResponse;
import com.sollasi.server.album.rest.response.AlbumResponse;
import com.sollasi.server.album.service.AlbumService;
import com.sollasi.server.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/api/album")
public class AlbumRest {

    @Autowired
    private AlbumService albumService;
    @Autowired
    private CustomerService customerService;

    @PostMapping("/")
    public AlbumResponse getAlbums(@RequestParam(value = "offset") int offset, @RequestParam(value = "limit") int limit,
                                   @RequestBody AlbumRequest request) throws Exception {
        return albumService.getAlbums(offset, limit, request.getArtistId());
    }

    @GetMapping("/{albumId}")
    public AlbumDetailResponse getTrackDetail(Principal principal, @PathVariable("albumId") Long albumId) throws Exception {
        return albumService.getAlbumDetail(principal.getName(), albumId);
    }

    @PostMapping("/like/{albumId}")
    public AlbumFeedbackResponse likeTrack(Principal principal, @PathVariable("albumId") Long albumId) throws Exception {
        return customerService.likeAlbum(principal.getName(), albumId);
    }

    @PostMapping("/dislike/{albumId}")
    public AlbumFeedbackResponse dislikeTrack(Principal principal, @PathVariable("albumId") Long albumId) throws Exception {
        return customerService.dislikeAlbum(principal.getName(), albumId);
    }


}
