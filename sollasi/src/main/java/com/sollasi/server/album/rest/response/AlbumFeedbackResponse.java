package com.sollasi.server.album.rest.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AlbumFeedbackResponse {
    private Long totalLike;
    private Long totalDislike;
}
