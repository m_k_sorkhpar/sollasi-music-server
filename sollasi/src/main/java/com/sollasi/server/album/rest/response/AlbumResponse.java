package com.sollasi.server.album.rest.response;

import com.sollasi.base.util.DateUtils;
import com.sollasi.server.album.domain.Album;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AlbumResponse {
    private long total;

    private List<AlbumResponseElement> albums;

    public void add(Album album) {
        if (albums == null) {
            albums = new ArrayList<>();
        }
        albums.add(new AlbumResponseElement(album));
    }

    @Data
    private class AlbumResponseElement {
        private Long id;

        private String name;

        private String publishDate;

        private String artist;

        private Long mediaId;

        public AlbumResponseElement(Album album) {
            this.id = album.getId();
            this.name = album.getName();
            this.publishDate = DateUtils.toPersianDateTime(album.getPublishDate());
            this.artist = album.getArtist() != null ? album.getArtist().getName() : null;
            this.mediaId = album.getImage() != null ? album.getImage().getId() : null;
        }
    }
}
