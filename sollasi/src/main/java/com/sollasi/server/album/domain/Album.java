package com.sollasi.server.album.domain;

import com.sollasi.server.artist.domain.Artist;
import com.sollasi.server.category.domain.Category;
import com.sollasi.server.media.domain.Media;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "album")
@Data
@Accessors
public class Album {

    @Id
    @Column(name = "album_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "like_counter", columnDefinition = "BIGINT(20) DEFAULT 0")
    private Long likeCounter;

    @Column(name = "dislike_counter", columnDefinition = "BIGINT(20) DEFAULT 0")
    private Long dislikeCounter;

    @Column(name = "creation_date", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private LocalDateTime creationDatetime = LocalDateTime.now();

    @Column(name = "publish_date")
    private LocalDate publishDate;

    @Column(name = "price")
    private Long price;

    @ManyToOne
    @JoinColumn(name = "artist_fk")
    private Artist artist;

    @ManyToOne
    @JoinColumn(name = "image_fk")
    private Media image;

    @ManyToMany
    @JoinTable(name = "album_m2m_categories", joinColumns = {
            @JoinColumn(name = "album_fk", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "category_fk",
                    nullable = false, updatable = false)})
    private List<Category> categories;

}

