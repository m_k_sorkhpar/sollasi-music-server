package com.sollasi.server.album.repository;

import com.sollasi.server.album.domain.Album;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AlbumRepository extends JpaRepository<Album, Long> {

    @Query("select a from Album a where a.price is not null" +
            " and(:artistId is null or a.artist.id = :artistId)" +
            " order by a.creationDatetime desc ")
    Page<Album> findAlbums(@Param("artistId") Long artistId, Pageable pageable);

    @Query(value = "select a from Album a join a.categories c where " +
            "a.name like concat('%',:word,'%') or " +
            "a.artist.name like concat('%',:word,'%') or " +
            "c.name like concat('%',:word,'%')")
//TODO order by
    Page<Album> search(@Param("word") String word, Pageable pageable);

}
