package com.sollasi.server.album.service;

import com.sollasi.base.advicers.SollasiExceptionHandler;
import com.sollasi.base.util.GridPagination;
import com.sollasi.security.baseuser.domain.Customer;
import com.sollasi.server.album.domain.Album;
import com.sollasi.server.album.repository.AlbumRepository;
import com.sollasi.server.album.rest.response.AlbumDetailResponse;
import com.sollasi.server.album.rest.response.AlbumResponse;
import com.sollasi.server.artist.service.ArtistService;
import com.sollasi.server.customer.domain.FeedbackType;
import com.sollasi.server.customer.domain.activities.AlbumFeedback;
import com.sollasi.server.customer.service.CustomerService;
import com.sollasi.server.track.rest.response.TrackResponse;
import com.sollasi.server.track.service.TrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public class AlbumService {
    @Autowired
    private AlbumRepository albumRepository;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private ArtistService artistService;
    @Autowired
    private TrackService trackService;

    public Album getAlbumEntity(Long albumId) throws Exception {
        Album album = albumRepository.findOne(albumId);
        if (album == null) {
            throw new Exception(SollasiExceptionHandler.ALBUM_NOT_FOUND);
        }
        return album;
    }

    public AlbumResponse getAlbums(int offset, int limit, Long artistId) throws Exception {
        AlbumResponse response = new AlbumResponse();

        if (artistId != null) {
            artistService.getArtistEntity(artistId);
        }

        Page<Album> albums = albumRepository.findAlbums(artistId, new GridPagination(offset, limit));
        for (Album album : albums) {
            response.add(album);
        }
        response.setTotal(albums.getTotalElements());
        return response;
    }

    public AlbumDetailResponse getAlbumDetail(String username, Long albumId) throws Exception {
        Customer customer = customerService.getCustomerEntityByUsername(username);
        Album album = getAlbumEntity(albumId);
        TrackResponse tracks = trackService.getTracks(0, 20, null, album.getId(), null);
        AlbumFeedback albumFeedback = customerService.getCustomerAlbumFeedback(customer, album);
        return new AlbumDetailResponse(album, tracks,
                customerService.isCustomerPurchasedAlbum(customer, album),
                albumFeedback != null && albumFeedback.getType() == FeedbackType.LIKE,
                albumFeedback != null && albumFeedback.getType() == FeedbackType.DISLIKE);

    }

    public void increaseLikeCounter(Album album) {
        album.setLikeCounter(album.getLikeCounter() + 1);
        albumRepository.save(album);
    }

    public void decreaseLikeCounter(Album album) {
        album.setLikeCounter(album.getLikeCounter() - 1);
        albumRepository.save(album);
    }

    public void increaseDislikeCounter(Album album) {
        album.setDislikeCounter(album.getDislikeCounter() + 1);
        albumRepository.save(album);
    }

    public void decreaseDislikeCounter(Album album) {
        album.setDislikeCounter(album.getDislikeCounter() - 1);
        albumRepository.save(album);
    }

    public AlbumResponse searchAlbum(Customer customer, String word) {
        AlbumResponse albumResponse = new AlbumResponse();
        Page<Album> albums = albumRepository.search(word, new GridPagination(0, 10));
        for (Album album : albums.getContent()) {
            albumResponse.add(album);
        }
        albumResponse.setTotal(albums.getContent().size());
        return albumResponse;
    }
}
