package com.sollasi.server.admin.domain;

import com.sollasi.security.baseuser.domain.BaseUser;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("admin")
@Data
@Accessors
public class Admin extends BaseUser {

}

