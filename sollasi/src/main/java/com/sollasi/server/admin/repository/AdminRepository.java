package com.sollasi.server.admin.repository;

import com.sollasi.security.baseuser.domain.BaseUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminRepository extends JpaRepository<BaseUser, Long> {
    BaseUser findByUsername(String userName);
}
