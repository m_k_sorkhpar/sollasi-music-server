package com.sollasi.server;

import com.sollasi.server.customer.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@Slf4j
public class IndexController {
    @Value("${IPG.provider}")
    private String IPGProvider;
    @Autowired
    private CustomerService customerService;


    @Autowired
    public IndexController() {
    }

    @GetMapping(value = {"/", "/index", "/index.html", "/index.jsp"})
    public String root() {
        return "index";
    }

    //IPG URL based on provider name
    @PostMapping(value = {"/arianpal"})
    public String IPGArianPalResult(@RequestParam("status") int status,
                                    @RequestParam("refnumber") String refNumber,
                                    @RequestParam("resnumber") Long financialTransactionId,
                                    ModelAndView modelAndView) throws Exception {
        logger.info("IPG response is : [status: [{}] , refNumber:[{}] , financialTransactionId:[{}] ]"
                , status, refNumber, financialTransactionId);
        if (!IPGProvider.equals("arianpal")) {
            logger.error("Arianpal send CallBack without permission");
            throw new Exception("Arianpal send CallBack without permission");
        }
        Long sollasiRefNo = customerService.arianpalCallback(financialTransactionId, status, refNumber);
        modelAndView.addObject("refNo", sollasiRefNo);
        return IPGProvider;
    }

    @PostMapping(value = {"/payIr"})
    public ModelAndView IPGPayIrResult(@RequestParam("status") int status,
                                       @RequestParam("transId") int transactionId,
                                       @RequestParam("factorNumber") Long financialTransactionId,
                                       @RequestParam(value = "mobile", required = false) String phoneNumber,
                                       @RequestParam(value = "cardNumber", required = false) String cardNumber,
                                       @RequestParam("message") String message) throws Exception {
        logger.info("payIr callback with values : [ status:{} , transactionId:{} , " +
                        "factorNumber:{} , mobile:{} , cardNumber:{}, message:{} ]",
                status, transactionId, financialTransactionId, phoneNumber,
                cardNumber, message);
        if (!IPGProvider.equals("payIr")) {
            logger.error("payIr send CallBack without permission");
            throw new Exception("payIr send CallBack without permission");
        }
        Long sollasiRefNo = customerService.payIrCallback(
                financialTransactionId, status, String.valueOf(transactionId), message
        );
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("refNo", sollasiRefNo);
        return modelAndView;
    }


}
