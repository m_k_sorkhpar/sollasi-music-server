package com.sollasi.server.track.rest.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TrackRequest {
    private Long artistId;
    private Long albumId;
    private Long categoryId;
}
