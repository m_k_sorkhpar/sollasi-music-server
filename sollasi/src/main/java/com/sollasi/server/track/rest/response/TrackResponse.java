package com.sollasi.server.track.rest.response;

import com.sollasi.base.util.DateUtils;
import com.sollasi.server.track.domain.Track;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class TrackResponse {
    private long total;
    private List<TrackResponseElement> tracks;

    public void add(Track track) {
        if (tracks == null) {
            tracks = new ArrayList<>();
        }
        tracks.add(new TrackResponseElement(track));
    }

    @Data
    @NoArgsConstructor
    public class TrackResponseElement {

        private Long id;

        private String name;

        private String publishDate;

        private String artist;

        private String album;

        private Long mediaId;

        private int durationSecond;


        public TrackResponseElement(Track track) {
            this.id = track.getId();
            this.name = track.getName();
            if (track.getPublishDate() != null) {
                this.publishDate = DateUtils.toPersianDateTime(track.getPublishDate());
            }
            this.artist = track.getArtist() != null ? track.getArtist().getName() : null;
            this.album = track.getAlbum() != null ? track.getAlbum().getName() : null;
            this.mediaId = track.getImage() != null ? track.getImage().getId() : null;
            this.durationSecond = track.getDurationSecond();
        }
    }
}
