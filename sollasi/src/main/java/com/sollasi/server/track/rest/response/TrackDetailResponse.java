package com.sollasi.server.track.rest.response;

import com.sollasi.base.util.DateUtils;
import com.sollasi.server.track.domain.Track;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TrackDetailResponse {

    public TrackDetailResponse(Track track, boolean isPurchased, boolean isLiked, boolean isDisliked) {

        this.id = track.getId();
        this.name = track.getName();
        if (track.getPublishDate() != null) {
            this.publishDate = DateUtils.toPersianDateTime(track.getPublishDate());
        }
        this.artist = track.getArtist() != null ? track.getArtist().getName() : null;
        this.album = track.getAlbum() != null ? track.getAlbum().getName() : null;
        this.mediaId = track.getImage() != null ? track.getImage().getId() : null;
        this.likeCounter = track.getLikeCounter();
        this.dislikeCounter = track.getDislikeCounter();
        this.price = track.getPrice();

        if (track.getCategories() != null && track.getCategories().size() != 0) {
            categories = new ArrayList<>();
            track.getCategories().forEach(category -> categories.add(category.getName()));
        }
        this.durationSecond = track.getDurationSecond();
        this.lyrics = track.getLyrics();
        this.purchased = isPurchased;
        this.liked = isLiked;
        this.disliked = isDisliked;
    }

    private Long id;

    private String name;

    private String publishDate;

    private String artist;

    private String album;

    private Long mediaId;

    private long likeCounter;

    private long dislikeCounter;

    private Long price;

    private List<String> categories;

    private int durationSecond;

    private String lyrics;

    private boolean liked;

    private boolean disliked;

    private boolean purchased;


}
