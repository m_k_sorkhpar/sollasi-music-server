package com.sollasi.server.track.repository;

import com.sollasi.server.track.domain.Track;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TrackRepository extends JpaRepository<Track, Long> {

    @Query("select t from Track t join t.categories c  where t.price is not null and " +
            "(:artistId is null or t.artist.id = :artistId ) and " +
            "(:albumId is null or t.album.id = :albumId ) and " +
            "(:categoryId is null or c.id=:categoryId) " +
            " order by t.creationDatetime desc ,t.name asc ")
    Page<Track> findTracks(@Param("artistId") Long artistId,
                           @Param("albumId") Long albumId,
                           @Param("categoryId") Long categoryId,
                           Pageable pageable);

    @Query("select t from Track t where t.price is not null  order by t.creationDatetime desc ")
    Page<Track> findTracks(Pageable pageable);

    @Query(value = "select t from Track t join t.categories c where  t.price is not null and " +
            "t.name like concat('%',:word,'%') or " +
            "t.artist.name like concat('%',:word,'%') or " +
            "c.name like concat('%',:word,'%') or " +
            "t.album.name like concat('%',:word,'%')")
//TODO order by
    Page<Track> search(@Param("word") String word, Pageable pageable);
}
