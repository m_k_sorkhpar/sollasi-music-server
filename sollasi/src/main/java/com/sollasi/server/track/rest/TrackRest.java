package com.sollasi.server.track.rest;

import com.sollasi.server.customer.service.CustomerService;
import com.sollasi.server.track.rest.request.TrackRequest;
import com.sollasi.server.track.rest.response.TrackDetailResponse;
import com.sollasi.server.track.rest.response.TrackFeedbackResponse;
import com.sollasi.server.track.rest.response.TrackResponse;
import com.sollasi.server.track.service.TrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.security.Principal;

@RestController
@RequestMapping("/api/track")
public class TrackRest {
    @Autowired
    private TrackService trackService;
    @Autowired
    private CustomerService customerService;


    @PostMapping("/")
    public TrackResponse getTracks(@RequestParam(value = "offset") int offset, @RequestParam(value = "limit") int limit,
                                   @RequestBody TrackRequest request) throws Exception {

        return trackService.getTracks(
                offset,
                limit,
                request.getArtistId(),
                request.getAlbumId(),
                request.getCategoryId()
        );
    }

    @GetMapping("/{trackId}")
    public TrackDetailResponse getTrackDetail(Principal principal, @PathVariable("trackId") Long trackId) throws Exception {
        return trackService.getTrackDetail(principal.getName(), trackId);
    }

    @PostMapping("/like/{trackId}")
    public TrackFeedbackResponse likeTrack(Principal principal, @PathVariable("trackId") Long trackId) throws Exception {
        return customerService.likeTrack(principal.getName(), trackId);
    }

    @PostMapping("/dislike/{trackId}")
    public TrackFeedbackResponse dislikeTrack(Principal principal, @PathVariable("trackId") Long trackId) throws Exception {
        return customerService.dislikeTrack(principal.getName(), trackId);
    }


    /*@GetMapping("/encrypted/{filename:.+}")
    public void fullEncryptedMusic(@PathVariable("filename") String id, HttpServletRequest request, HttpServletResponse response
    ) throws Exception {
        Path file = new File(musicPath + File.separator + id )
                .toPath();
        MultipartFileSender.fromPath(
                file)
                .with(request)
                .with(response)
                .serveResource("test09113119055");
    }*/

    @GetMapping("/stream/{trackId}")
    public void streamMusic(@PathVariable("trackId") Long trackId,
                            @RequestParam("username") String username,
                            HttpServletRequest request, HttpServletResponse response
    ) throws Exception {
        if (!username.startsWith("\\+")) {
            username = "+" + username.trim();
        }
        String trackPath = trackService.getTrackPath(trackId, username);
        MultipartFileSender.fromPath(new File(trackPath).toPath())
                .with(request)
                .with(response)
                .serveResource(null);
    }

}
