package com.sollasi.server.track.rest.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TrackFeedbackResponse {
    private Long totalLike;
    private Long totalDislike;
}
