package com.sollasi.server.track.repository;

import com.sollasi.server.track.domain.PromotedTrack;
import com.sollasi.server.track.domain.Track;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PromotedTrackRepository extends JpaRepository<PromotedTrack, Long> {
}
