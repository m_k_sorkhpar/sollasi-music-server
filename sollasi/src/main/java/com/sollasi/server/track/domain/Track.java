package com.sollasi.server.track.domain;

import com.sollasi.server.album.domain.Album;
import com.sollasi.server.artist.domain.Artist;
import com.sollasi.server.category.domain.Category;
import com.sollasi.server.media.domain.Media;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "track")
@Data
@Accessors
public class Track {

    @Id
    @Column(name = "track_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "like_counter", columnDefinition = "BIGINT(20) DEFAULT 0")
    private long likeCounter;

    @Column(name = "dislike_counter", columnDefinition = "BIGINT(20) DEFAULT 0")
    private long dislikeCounter;

    @Column(name = "creation_date", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private LocalDateTime creationDatetime = LocalDateTime.now();

    @Column(name = "publish_date")
    private LocalDate publishDate;

    @Column(name = "price")
    private Long price;

    @ManyToOne(optional = false)
    @JoinColumn(name = "artist_fk")
    private Artist artist;

    @ManyToOne
    @JoinColumn(name = "album_fk")
    private Album album;

    @Column(name = "track_duration_second", nullable = false)
    private Integer durationSecond;

    @ManyToOne
    @JoinColumn(name = "image_fk")
    private Media image;

    @Column(name = "file_path", nullable = false)
    private String path;

    @Column(name = "demo_file_path")
    private String demoPath;

    @Column(name = "track_lyrics", columnDefinition = "TEXT")
    private String lyrics;


    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "track_m2m_categories", joinColumns = {
            @JoinColumn(name = "track_fk", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "category_fk",
                    nullable = false, updatable = false)})
    private List<Category> categories;

}

