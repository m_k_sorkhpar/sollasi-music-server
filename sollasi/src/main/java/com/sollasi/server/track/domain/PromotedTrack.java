package com.sollasi.server.track.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Entity
@Table(name = "promoted_track")
@Data
@Accessors
public class PromotedTrack {

    @Id
    @Column(name = "promoted_track_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "track_fk")
    private Track track;

    @Column(name = "priority", nullable = false,unique = true)
    private Integer priority;

}

