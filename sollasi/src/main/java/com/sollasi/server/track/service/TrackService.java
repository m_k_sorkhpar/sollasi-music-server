package com.sollasi.server.track.service;

import com.sollasi.base.advicers.SollasiExceptionHandler;
import com.sollasi.base.util.GridPagination;
import com.sollasi.security.baseuser.domain.Customer;
import com.sollasi.server.album.service.AlbumService;
import com.sollasi.server.artist.service.ArtistService;
import com.sollasi.server.category.service.CategoryService;
import com.sollasi.server.customer.domain.FeedbackType;
import com.sollasi.server.customer.domain.activities.TrackFeedback;
import com.sollasi.server.customer.repository.AlbumPurchasedRepository;
import com.sollasi.server.customer.repository.TrackPurchasedRepository;
import com.sollasi.server.customer.service.CustomerService;
import com.sollasi.server.playlist.service.PlayListService;
import com.sollasi.server.track.domain.PromotedTrack;
import com.sollasi.server.track.domain.Track;
import com.sollasi.server.track.repository.PromotedTrackRepository;
import com.sollasi.server.track.repository.TrackRepository;
import com.sollasi.server.track.rest.response.TrackDetailResponse;
import com.sollasi.server.track.rest.response.TrackResponse;
import lombok.extern.slf4j.Slf4j;
import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.builder.FFmpegBuilder;
import net.bramp.ffmpeg.probe.FFmpegProbeResult;
import net.bramp.ffmpeg.progress.Progress;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class TrackService {
    @Autowired
    private TrackRepository trackRepository;
    @Autowired
    private PromotedTrackRepository promotedTrackRepository;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private AlbumService albumService;
    @Autowired
    private ArtistService artistService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private PlayListService playListService;
    @Autowired
    private TrackPurchasedRepository trackPurchasedRepository;
    @Autowired
    private AlbumPurchasedRepository albumPurchasedRepository;


    @Value("${com.sollasi.upload.music}")
    private String musicPath;

    @Value("${com.sollasi.upload.demo}")
    private String demoPath;

    @Value("${com.sollasi.ffmpegPath}")
    private String ffmpegPath;

    @Value("${com.sollasi.ffprobePath}")
    private String ffprobePath;

    private static FFmpeg ffmpeg;
    private static FFprobe ffprobe;

    @PostConstruct
    public void init() {
        try {
            ffmpeg = new FFmpeg(ffmpegPath);
            ffprobe = new FFprobe(ffprobePath);
        } catch (Exception e) {
            logger.error("initiating of ffmpeg throw exception", e);
        }
    }

    private HashSet<String> userStreamAccess = new HashSet<>();
    private HashMap<Long, String> streamPaths = new HashMap<>();
    private HashMap<Long, String> demoPaths = new HashMap<>();
    private HashMap<Long, String> freeTracksPath = new HashMap<>();

    public Track getTrackEntity(Long trackId) throws Exception {
        Track track = trackRepository.findOne(trackId);
        if (track == null) {
            throw new Exception(SollasiExceptionHandler.TRACK_NOT_FOUND);
        }
        return track;
    }

    public TrackResponse getTracks(int offset, int limit, Long artistId, Long albumId, Long categoryId)
            throws Exception {
        TrackResponse response = new TrackResponse();
        if (artistId != null) {
            artistService.getArtistEntity(artistId);
        }
        if (albumId != null) {
            albumService.getAlbumEntity(albumId);
        }
        if (categoryId != null) {
            categoryService.getCategoryTracks(categoryId);
        }

        Page<Track> tracks = null;
        if (artistId == null && albumId == null && categoryId == null) {
            tracks = trackRepository.findTracks(new GridPagination(offset, limit));
        } else {
            tracks = trackRepository.findTracks(artistId, albumId, categoryId, new GridPagination(offset, limit));
        }
        for (Track track : tracks) {
            response.add(track);
        }
        response.setTotal(tracks.getTotalElements());
        return response;
    }

    public TrackResponse getPromotedTrack() {
        List<PromotedTrack> promotedTrackList = promotedTrackRepository.findAll(new Sort(Sort.Direction.DESC, "priority"));
        TrackResponse response = new TrackResponse();
        for (PromotedTrack promotedTrack : promotedTrackList) {
            response.add(promotedTrack.getTrack());
        }
        response.setTotal(promotedTrackList.size());
        return response;
    }

    public TrackDetailResponse getTrackDetail(String username, Long trackId) throws Exception {
        Customer customer = customerService.getCustomerEntityByUsername(username);
        Track track = getTrackEntity(trackId);
        TrackFeedback trackFeedback = customerService.getCustomerTrackFeedback(customer, track);
        return new TrackDetailResponse(track,
                customerService.isCustomerPurchasedTrack(customer, track),
                trackFeedback != null && trackFeedback.getType() == FeedbackType.LIKE,
                trackFeedback != null && trackFeedback.getType() == FeedbackType.LIKE
        );
    }

    public String getTrackPath(Long trackId, String username) throws Exception {
        if (streamPaths.get(trackId) == null) {
            Track track = getTrackEntity(trackId);
            if (track.getPath() != null && !StringUtils.isEmpty(track.getPath())) {
                streamPaths.put(trackId, musicPath + File.separator + track.getPath());
            }
            if (track.getPrice() == 0) {
                freeTracksPath.put(trackId, musicPath + File.separator + track.getPath());
            }
        }
        if (freeTracksPath.containsKey(trackId)) {
            return freeTracksPath.get(trackId);
        }
        if (!userStreamAccess.contains(trackId + username)) {
            if (!customerService.isCustomerPurchasedTrack(username, trackId)) {
                return getDemoTrackPath(trackId);
            } else {
                userStreamAccess.add(trackId + username);
            }
        }
        return streamPaths.get(trackId);
    }


    public String getDemoTrackPath(Long trackId) throws Exception {
        if (demoPaths.get(trackId) == null) {
            Track track = getTrackEntity(trackId);
            if (!(track.getDemoPath() != null && !StringUtils.isEmpty(track.getDemoPath()))) {
                FFmpegProbeResult in = ffprobe.probe(musicPath + File.separator + track.getPath());
                FFmpegBuilder builder = new FFmpegBuilder()
                        .setInput(in)
                        .overrideOutputFiles(true)
                        .addOutput(demoPath + File.separator + track.getPath())
                        .setStartOffset(0, TimeUnit.SECONDS)
                        .setDuration(30, TimeUnit.SECONDS)
                        .done();
                FFmpegExecutor executor = new FFmpegExecutor(ffmpeg, ffprobe);
                executor.createJob(builder, progress -> {
                    if (progress.status == Progress.Status.END) {
                        logger.info("converting trackId:{} to short 30 sec video is done", trackId);
                    }
                }).run();
                track.setDemoPath(track.getPath());
                trackRepository.save(track);
            }
            demoPaths.put(trackId, demoPath + File.separator + track.getDemoPath());
        }

        return demoPaths.get(trackId);
    }

    @Scheduled(fixedDelay = 10 * 60 * 1000)
    public void clearCaches() {
        this.userStreamAccess.clear();
        this.streamPaths.clear();
        this.demoPaths.clear();
    }

    public void increaseLikeCounter(Track track) {
        track.setLikeCounter(track.getLikeCounter() + 1);
        trackRepository.save(track);
    }

    public void decreaseLikeCounter(Track track) {
        track.setLikeCounter(track.getLikeCounter() - 1);
        trackRepository.save(track);
    }

    public void increaseDislikeCounter(Track track) {
        track.setDislikeCounter(track.getDislikeCounter() + 1);
        trackRepository.save(track);
    }

    public void decreaseDislikeCounter(Track track) {
        track.setDislikeCounter(track.getDislikeCounter() - 1);
        trackRepository.save(track);
    }

    public TrackResponse searchTrack(Customer customer, String word) {
        TrackResponse trackResponse = new TrackResponse();
        Page<Track> tracks = trackRepository.search(word, new GridPagination(0, 10));
        for (Track track : tracks.getContent()) {
            trackResponse.add(track);
        }
        trackResponse.setTotal(tracks.getContent().size());
        return trackResponse;
    }
}
