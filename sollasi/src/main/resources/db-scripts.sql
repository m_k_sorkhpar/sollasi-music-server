ALTER DATABASE `taraneh_sora`
CHARACTER SET utf8
COLLATE utf8_general_ci;

ALTER TABLE `album`
  CONVERT TO CHARACTER SET utf8
  COLLATE utf8_unicode_ci;
ALTER TABLE `artist`
  CONVERT TO CHARACTER SET utf8
  COLLATE utf8_unicode_ci;
ALTER TABLE `base_user`
  CONVERT TO CHARACTER SET utf8
  COLLATE utf8_unicode_ci;
ALTER TABLE `category`
  CONVERT TO CHARACTER SET utf8
  COLLATE utf8_unicode_ci;
ALTER TABLE `news`
  CONVERT TO CHARACTER SET utf8
  COLLATE utf8_unicode_ci;
ALTER TABLE `playlist`
  CONVERT TO CHARACTER SET utf8
  COLLATE utf8_unicode_ci;
ALTER TABLE `track`
  CONVERT TO CHARACTER SET utf8
  COLLATE utf8_unicode_ci;
ALTER TABLE `financial_transaction`
  CONVERT TO CHARACTER SET utf8
  COLLATE utf8_unicode_ci;

INSERT INTO `base_user` (user_type, activated, activation_key, email, first_name, gender, lang_key, last_name, last_password_reset_date, password, phone_number, reset_key, status, username, credit, vip_expiration_date)
VALUES ('customer', TRUE, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ACTIVATE', '+989113119055', 0,
        NULL);

INSERT INTO `users_authorities` VALUE (1, 'customer');

INSERT INTO `users_m2m_authorities` (user_fk, user_authorities_fk) VALUES (1, 1);

INSERT INTO `media_files` (extension, mime_type, media_path, has_public_access, small_media_path)
VALUES (NULL, NULL, '1.jpg', 1, NULL);

INSERT INTO `artist` (bio, name, image_fk) VALUES ('یه متن چرت', 'محسن یگانه', 1);
INSERT INTO `artist` (bio, name, image_fk) VALUES ('یه متن چرت', 'احمد سعدی', 1);
INSERT INTO `artist` (bio, name, image_fk) VALUES ('یه متن چرت', 'یعقوب', 1);


INSERT INTO `album` (creation_date, dislike_counter, like_counter, name, price, publish_date, artist_fk, image_fk)
VALUES ('2018-06-08 18:19:34', 1, 50, 'حباب', 6000, '2018-06-07', 1, 1);
INSERT INTO `album` (creation_date, dislike_counter, like_counter, name, price, publish_date, artist_fk, image_fk)
VALUES ('2018-06-08 18:20:51', 10, 30, 'بارون', 5000, '2018-06-03', 2, 1);


INSERT INTO `category` (category_id, name, image_fk) VALUES (1, 'pop', 1);
INSERT INTO `category` (category_id, name, image_fk) VALUES (2, 'سنتی', 1);


INSERT INTO `album_m2m_categories` (album_fk, category_fk) VALUES (1, 1);
INSERT INTO `album_m2m_categories` (album_fk, category_fk) VALUES (2, 1);


INSERT INTO `track` (creation_date, demo_file_path, dislike_counter, like_counter, name, file_path, price, publish_date, album_fk, artist_fk, image_fk, track_duration_second)
VALUES ('2018-06-08 18:12:34', NULL, 10, 100, 'نرو', '1.mp3', 700, '2018-06-08', 1, 1, 1, 423);
INSERT INTO `track` (creation_date, demo_file_path, dislike_counter, like_counter, name, file_path, price, publish_date, album_fk, artist_fk, image_fk, track_duration_second)
VALUES ('2018-06-08 18:12:34', NULL, 10, 100, '۲نرو', '1.mp3', 700, '2018-06-08', 1, 1, 1, 323);
INSERT INTO `track` (creation_date, demo_file_path, dislike_counter, like_counter, name, file_path, price, publish_date, album_fk, artist_fk, image_fk, track_duration_second)
VALUES ('2018-06-08 18:12:34', NULL, 10, 100, '۳نرو', '1.mp3', 700, '2018-06-08', 1, 1, 1, 523);
INSERT INTO `track` (creation_date, demo_file_path, dislike_counter, like_counter, name, file_path, price, publish_date, album_fk, artist_fk, image_fk, track_duration_second)
VALUES ('2018-06-08 18:12:34', NULL, 10, 100, '۴نرو', '1.mp3', 700, '2018-06-08', 1, 1, 1, 413);
INSERT INTO `track` (creation_date, demo_file_path, dislike_counter, like_counter, name, file_path, price, publish_date, album_fk, artist_fk, image_fk, track_duration_second)
VALUES ('2018-06-08 18:12:34', NULL, 10, 100, '۵نرو', '1.mp3', 700, '2018-06-08', 1, 1, 1, 363);
INSERT INTO `track` (creation_date, demo_file_path, dislike_counter, like_counter, name, file_path, price, publish_date, album_fk, artist_fk, image_fk, track_duration_second)
VALUES ('2018-06-08 18:12:34', NULL, 10, 100, '۶نرو', '1.mp3', 700, '2018-06-08', 1, 1, 1, 193);
INSERT INTO `track` (creation_date, demo_file_path, dislike_counter, like_counter, name, file_path, price, publish_date, album_fk, artist_fk, image_fk, track_duration_second)
VALUES ('2018-06-08 18:12:34', NULL, 10, 100, '۷نرو', '1.mp3', 700, '2018-06-08', 1, 1, 1, 393);
INSERT INTO `track` (creation_date, demo_file_path, dislike_counter, like_counter, name, file_path, price, publish_date, album_fk, artist_fk, image_fk, track_duration_second)
VALUES ('2018-06-08 18:12:34', NULL, 10, 100, '۸نرو', '1.mp3', 700, '2018-06-08', 1, 1, 1, 551);
INSERT INTO `track` (creation_date, demo_file_path, dislike_counter, like_counter, name, file_path, price, publish_date, album_fk, artist_fk, image_fk, track_duration_second)
VALUES ('2018-06-08 18:12:34', NULL, 10, 100, '۹نرو', '1.mp3', 700, '2018-06-08', 1, 1, 1, 410);
INSERT INTO `track` (creation_date, demo_file_path, dislike_counter, like_counter, name, file_path, price, publish_date, album_fk, artist_fk, image_fk, track_duration_second)
VALUES ('2018-06-08 18:12:34', NULL, 10, 100, '۱۰نرو', '1.mp3', 700, '2018-06-08', 1, 1, 1, 280);
INSERT INTO `track` (creation_date, demo_file_path, dislike_counter, like_counter, name, file_path, price, publish_date, album_fk, artist_fk, image_fk, track_duration_second)
VALUES ('2018-06-08 18:23:52', NULL, 8, 200, 'بارون رو دوست دارم', '11.mp3', 900, '2018-06-29', 2, 2, 1, 345);


INSERT INTO `track_m2m_categories` (track_fk, category_fk) VALUES (1, 1);
INSERT INTO `track_m2m_categories` (track_fk, category_fk) VALUES (1, 2);
INSERT INTO `track_m2m_categories` (track_fk, category_fk) VALUES (2, 1);
INSERT INTO `track_m2m_categories` (track_fk, category_fk) VALUES (3, 1);
INSERT INTO `track_m2m_categories` (track_fk, category_fk) VALUES (4, 1);
INSERT INTO `track_m2m_categories` (track_fk, category_fk) VALUES (5, 1);
INSERT INTO `track_m2m_categories` (track_fk, category_fk) VALUES (6, 1);
INSERT INTO `track_m2m_categories` (track_fk, category_fk) VALUES (7, 1);
INSERT INTO `track_m2m_categories` (track_fk, category_fk) VALUES (8, 1);
INSERT INTO `track_m2m_categories` (track_fk, category_fk) VALUES (9, 1);
INSERT INTO `track_m2m_categories` (track_fk, category_fk) VALUES (10, 1);
INSERT INTO `track_m2m_categories` (track_fk, category_fk) VALUES (11, 1);


INSERT INTO `promoted_track` (priority, track_fk) VALUES (1, 10);
INSERT INTO `promoted_track` (priority, track_fk) VALUES (4, 11);
INSERT INTO `promoted_track` (priority, track_fk) VALUES (2, 3);
INSERT INTO `promoted_track` (priority, track_fk) VALUES (3, 4);


INSERT INTO `playlist` (name, customer_fk) VALUES ('پاپ ارش', 2);
INSERT INTO `playlist` (name, customer_fk) VALUES ('پاپ سرخپر', 1);
INSERT INTO `playlist` (name, customer_fk) VALUES ('پاپ ۲ سرخپر', 1);

INSERT INTO `playlist_m2m_track` (playlist_fk, track_fk) VALUES (1, 1);
INSERT INTO `playlist_m2m_track` (playlist_fk, track_fk) VALUES (1, 2);
INSERT INTO `playlist_m2m_track` (playlist_fk, track_fk) VALUES (1, 3);
INSERT INTO `playlist_m2m_track` (playlist_fk, track_fk) VALUES (1, 4);
INSERT INTO `playlist_m2m_track` (playlist_fk, track_fk) VALUES (1, 5);
INSERT INTO `playlist_m2m_track` (playlist_fk, track_fk) VALUES (1, 6);
INSERT INTO `playlist_m2m_track` (playlist_fk, track_fk) VALUES (1, 7);
INSERT INTO `playlist_m2m_track` (playlist_fk, track_fk) VALUES (1, 8);
INSERT INTO `playlist_m2m_track` (playlist_fk, track_fk) VALUES (1, 9);
INSERT INTO `playlist_m2m_track` (playlist_fk, track_fk) VALUES (1, 10);
INSERT INTO `playlist_m2m_track` (playlist_fk, track_fk) VALUES (1, 11);
INSERT INTO `playlist_m2m_track` (playlist_fk, track_fk) VALUES (2, 1);
INSERT INTO `playlist_m2m_track` (playlist_fk, track_fk) VALUES (2, 11);


INSERT INTO `album_purchased` (customer_new_credit, customer_previous_credit, submit_date, album_fk, customer_fk)
VALUES (10000, 11000, '2018-06-08 22:06:09', 1, 1);

INSERT INTO `track_purchased` (customer_new_credit, customer_previous_credit, submit_date, customer_fk, track_fk)
VALUES (9000, 10000, '2018-06-08 22:03:40', 1, 11);
INSERT INTO `track_purchased` (customer_new_credit, customer_previous_credit, submit_date, customer_fk, track_fk)
VALUES (11000, 12000, '2018-06-08 22:03:40', 2, 11);
INSERT INTO `track_purchased` (customer_new_credit, customer_previous_credit, submit_date, customer_fk, track_fk)
VALUES (10000, 11000, '2018-06-08 22:03:40', 2, 1);


INSERT INTO taraneh_sora.media_files (extension, mime_type, media_path, has_public_access)
VALUES ('.jpg', 'image/jpeg', '2.jpg', 1);
INSERT INTO taraneh_sora.media_files (extension, mime_type, media_path, has_public_access)
VALUES ('.jpg', 'image/jpeg', '3.jpg', 1);
INSERT INTO taraneh_sora.media_files (extension, mime_type, media_path, has_public_access)
VALUES ('.jpg', 'image/jpeg', '4.jpg', 1);


INSERT INTO taraneh_sora.news (content, creation_datetime, dislike_counter, like_counter, title, image_fk)
VALUES ('شماره جدید مجله «اینترتینمنت ویکلی» به کلی به فیلم «آکوامن»‌ (Aquaman) تازه‌ترین اثر استودیوی برادران وارنر در دنیای سینمایی دی‌سی اختصاص دارد و در آن از اولین تصاویر رسمی شخصیت «کوئین آتلانتا» مادر آکوامن با بازی «نیکول کیدمن» رونمایی شده است. «جیسون موموآ» ابرقهرمان خالکوبی شده در دو کاور اختصاصی مجله به چشم می‌خورد، یک کاور با تصویری از چهره درهم موموآ برای مشترکین و کاور دیگر با تصاویری از کیدمن، موموآ و «امبر هرد» برای روزنامه فروشی‌ها.

فیلم «آکوامن» که اولین فیلمی است که پس از «لیگ عدالت» روی پرده می‌رود، وقایعی را روایت می‌کند که بعد از برپایی «لیگ عدالت» و نجات دنیا توسط آکوامن اتفاق می‌افتد. «آرتور کری» (آکوامن) در این فیلم در نبری میان آتلانتین‌ها و «بلک مانتا» و «اوشن مستر» گرفتار می‌شود. پس از انتشار این تصاویر انتظار می‌رود اولین تریلر فیلم هم به زودی عرضه شود.

فیلم ۱۶۰ میلیون دلاری «آکوامن» به کارگردانی «جیمز ون» و بازی «جیسون موموآ»، «امبر هرد»،‌ «پاتریک ویلسون»، «یحیی عبدالامین»، «ویلیام دافو»، «دولف لاندگرن»، «لودی لین»، «تموریا موریسون»، و «نیکول کیدمن» ۲۱ دسامبر امسال به اکران سینماهای ایالات متحده درمی‌آید.',
        '2018-06-16 08:23:51', 1, 9, 'aquaman', 2);
INSERT INTO taraneh_sora.news (content, creation_datetime, dislike_counter, like_counter, title, image_fk)
VALUES (
  'گری راس، کارگردان ۶۱ ساله‌ایست که از ۱۸۹۶ تاکنون فیلم می‌سازد و تا پیش از این سابقه ساخت عناوینی چون «هانگر گیمز» و «دولت آزاد جونز» را داشته و در جدیدترین اقدام سینمایی خود به سراغ ساخت فیلمی به نام «۸ یار اوشن» (Ocean’s 8) رفته است. فیلمی درام و جنایی که یک اسپین‌آف از سه‌گانه «یاران اوشن» به شمار می‌رود که سال‌ها پیش استیون سودربرگ کارگردانی آن را برعهده داشت. داستان اصلی این فیلم پیرامون گروهی از زنان قانون‌گریز به سرکردگی شخصیتی به اسم «دبی اوشن» است که خواهر «دنی اوشن» در سه گانه یاران اوشن است. این گروه از زنان در تلاشند تا بزرگترین سرقت تاریخ را ترتیب دهند.',
  '2018-06-16 08:24:52', 2, 3, ' پرونده فیلم Ocean’s 8؛ فستیوال قرایح تباه‌شده', 3);
INSERT INTO taraneh_sora.news (content, creation_datetime, dislike_counter, like_counter, title, image_fk)
VALUES ('دو سریال کنسل شده امسال شبکه فاکس پس از تبدیل شدن به جنجالی‌ترین مباحث در توئیتر، توسط شبکه‌های دیگر خریداری شدند.

نت‌فلیکس برای تولید فصل چهارم سریال فانتزی «لوسیفر» قرارداد بست. «لوسیفر» با بازی «تام الیس»، به سریال «بروکلین نه-نه» می‌پیوندد که پس از اعلام خبر توقف تولیدش در شبکه فاکس به شبکه NBC سپرده شد. تصمیم نت‌فلیکس پس از کمپین عظیمی که در یک ماه گذشته با هشتگ #SaveLucifer در توئیتر و دیگر شبکه‌های اجتماعی شکل گرفت اتخاد می‌شود.

آمازون رقیب سرویس نت‌فلیکس یکی دیگر از مشتریان این سریال پربیننده بود. امتیاز پخش اینترنتی سه فصل نخست سریال «لوسیفر» در ایالات متحده در اختیار سرویس هولو بوده و همین مسئله موجب به وجود آمدن تأخیر در امضای این قرارداد شده است. چیزی که مسئله را پیچیده‌تر می‌کند این است که «لوسیفر» از طریق شبکه‌ها و پلتفورم‌های مختلفی پخش می‌شده است. برای مثال، آمازون این سریال را در اختیار بینندگان آلمانی و بریتانیایی قرار داده است. براساس آخرین گزارشات، از این پس نت‌فلیکس «لوسیفر» را در بریتانیا پخش می‌کند.

نت‌فلیکس برای خرید سریال کنسل شده دیگری به نام «Designated Survivor» هم در حال مذاکره است.

در سریال «لوسیفر» که براساس کتاب مصوری نوشته «نیل گیمن» ساخته شده، «تام الیس»، «لورن جرمن»، «ریچل هریس»، «دی‌بی وودساید»، «لزلی-ان برانت»، «اسکارلت استوز»، «کوین الهاندرو» و «ایمی گارسیا»، به ایفای نقش پرداخته‌اند. فصل سوم سریال «لوسیفر»، داستان «لوسیفر مورنینگ‌استار» (الیس) یا ارباب جهنم را روایت می‌کرد که در هیئت یک انسان خوش سیما و جذاب، به پلیس لوس آنجلس در به دام انداختن جنایاتکاران یاری می‌رساند.',
        '2018-06-17 08:25:32', 0, 12, ' نت‌فلیکس «لوسیفر» را نجات داد؛ سریال کنسل شده شبکه فاکس ادامه می‌یابد', 4);


CREATE USER 'panel'@'localhost'
  IDENTIFIED BY 'nuHF89NAMNwTkGGb47khWU8Cu5zNB9cz';


GRANT SELECT
ON taraneh_sora.activation_code TO 'panel'@'localhost';

ALTER TABLE album
  ADD is_deleted BOOLEAN DEFAULT FALSE NOT NULL;
GRANT SELECT,
INSERT (artist_fk, name, price, publish_date, image_fk),
UPDATE (artist_fk, name, price, publish_date, image_fk, is_deleted),
ON taraneh_sora.album TO 'panel'@'localhost';


GRANT SELECT
ON taraneh_sora.album_purchased TO 'panel'@'localhost';

GRANT SELECT
ON taraneh_sora.album_feedback TO 'panel'@'localhost';

GRANT SELECT, INSERT, UPDATE, DELETE
ON taraneh_sora.album_m2m_categories TO 'panel'@'localhost';

ALTER TABLE artist
  ADD is_deleted BOOLEAN DEFAULT FALSE NOT NULL;
GRANT SELECT, INSERT (image_fk, name, income_factor, bio), UPDATE (image_fk, name, income_factor, bio, is_deleted)
ON taraneh_sora.artist TO 'panel'@'localhost';


GRANT SELECT, UPDATE (id, last_name, first_name, activated, gender, email,
                      password, last_password_reset_date, lang_key,
                      status, vip_expiration_date)
ON taraneh_sora.base_user TO 'panel'@'localhost';

ALTER TABLE category
  ADD is_deleted BOOLEAN DEFAULT FALSE NOT NULL;
GRANT SELECT, INSERT, UPDATE
ON taraneh_sora.category TO 'panel'@'localhost';

GRANT SELECT
ON taraneh_sora.financial_transaction TO 'panel'@'localhost';

ALTER TABLE news
  ADD is_deleted BOOLEAN DEFAULT FALSE NOT NULL;

GRANT SELECT, INSERT, UPDATE
ON taraneh_sora.news TO 'panel'@'localhost';

GRANT SELECT, INSERT, UPDATE
ON taraneh_sora.hot_news TO 'panel'@'localhost';

GRANT SELECT, INSERT, UPDATE, DELETE
ON taraneh_sora.media_files TO 'panel'@'localhost';

GRANT SELECT
ON taraneh_sora.playlist TO 'panel'@'localhost';

GRANT SELECT
ON taraneh_sora.playlist_m2m_track TO 'panel'@'localhost';


ALTER TABLE track
  ADD is_deleted BOOLEAN DEFAULT FALSE NOT NULL;
GRANT SELECT,
INSERT (name, image_fk, album_fk, artist_fk, price, publish_date, demo_file_path,
        file_path, track_duration_second, track_lyrics),
UPDATE (name, image_fk, album_fk, artist_fk, price, publish_date, demo_file_path,
        file_path, track_duration_second, track_lyrics, is_deleted)
ON taraneh_sora.track TO 'panel'@'localhost';

GRANT SELECT, INSERT, UPDATE, DELETE
ON taraneh_sora.promoted_track TO 'panel'@'localhost';

GRANT SELECT
ON taraneh_sora.track_feedback TO 'panel'@'localhost';

GRANT SELECT, INSERT, UPDATE, DELETE
ON taraneh_sora.track_m2m_categories TO 'panel'@'localhost';

GRANT SELECT
ON taraneh_sora.track_purchased TO 'panel'@'localhost';

GRANT SELECT
ON taraneh_sora.users_authorities TO 'panel'@'localhost';

GRANT SELECT, INSERT
ON taraneh_sora.users_m2m_authorities TO 'panel'@'localhost';

ALTER TABLE base_user
  ADD CONSTRAINT check_user_gender_enum CHECK (gender IN ('WOMAN', 'MAN'));

ALTER TABLE base_user
  ADD CONSTRAINT check_user_status_enum CHECK (status IN ('ACTIVE',
                                                          'DEACTIVATED',
                                                          'NOT_COMPLETED',
                                                          'NOT_CHECKED',
                                                          'REJECTED')
);

ALTER TABLE album_feedback
  ADD CONSTRAINT check_album_feedback_enum CHECK (type IN ('LIKE',
                                                           'DISLIKE')
);

ALTER TABLE financial_transaction
  ADD CONSTRAINT check_transaction_type_enum CHECK (type IN ('BUY_VIP_ACCOUNT',
                                                             'INCREASE_CREDIT')
);

ALTER TABLE financial_transaction
  ADD CONSTRAINT check_transaction_status_enum CHECK (status IN ('INITIATE',
                                                                 'SUCCESSFUL',
                                                                 'FAILED',
                                                                 'ROLLBACK')
);


ALTER TABLE track_feedback
  ADD CONSTRAINT check_track_feedback_enum CHECK (type IN ('LIKE',
                                                           'DISLIKE')
);


ALTER TABLE track_feedback
  ADD CONSTRAINT check_track_feedback_enum CHECK (type IN ('LIKE',
                                                           'DISLIKE')
);


CREATE TABLE ticket
(
  ticket_id          BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
  ticket_subject     VARCHAR(255)                                                      NOT NULL,
  ticket_body        TEXT                                                              NOT NULL,
  parent_ticket_fk   BIGINT(20),
  user_fk            BIGINT(20)                                                        NOT NULL,
  ticket_submit_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP                               NOT NULL,
  ticket_status      VARCHAR(255) DEFAULT 'WAITING_FOR_EMPLOYEE_RESPONSE'              NOT NULL,
  image_fk           BIGINT(20),
  CONSTRAINT ticket_user_id_to_user_fk
  FOREIGN KEY (user_fk) REFERENCES base_user (id)
);


GRANT SELECT, INSERT, UPDATE, DELETE
ON `ticket` TO 'panel'@'localhost';

ALTER TABLE `ticket`
  CONVERT TO CHARACTER SET utf8
  COLLATE utf8_unicode_ci;

CREATE INDEX ticket_user_id_to_user_fk
  ON ticket (user_fk);

ALTER TABLE ticket
  ADD CONSTRAINT ticket_ticket_ticket_id_fk
FOREIGN KEY (parent_ticket_fk) REFERENCES ticket (ticket_id);

CREATE INDEX ticket_ticket_ticket_id_fk
  ON ticket (parent_ticket_fk);

ALTER TABLE ticket
  ADD CONSTRAINT check_ticket_status_enum CHECK (ticket_status IN ('CLOSED',
                                                                   'WAITING_FOR_EMPLOYEE_RESPONSE',
                                                                   'WAITING_FOR_CUSTOMER_RESPONSE'
)
);

ALTER TABLE track
  MODIFY creation_date TIMESTAMP DEFAULT current_timestamp;

ALTER TABLE album
  MODIFY creation_date TIMESTAMP DEFAULT current_timestamp;

ALTER TABLE news
  MODIFY creation_datetime TIMESTAMP DEFAULT current_timestamp;

ALTER TABLE track
  MODIFY track_lyrics TEXT;


ALTER TABLE track
  MODIFY track_duration_second INT(11) NOT NULL;



