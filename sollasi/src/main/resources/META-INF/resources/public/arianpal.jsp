<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<body>
<c:if test="${refNo}">
    پرداخت با موفق انجام شد. شماره پیگیری شما:
    <br>
    <h2>${refNo}</h2>
</c:if>
</body>
</html>